Manual acceptance tests

Person
1. Create a new person
    - They appear on the person list
    - first and last name are required
    - The person list is resorted alphabetically, not considering case

2. Edit a person name (which associations)
    - The person's name is changed in the associated transaction
    - The person's name is changed in the associated timelog


3. View the person timelogs list
    - A listing of their timelogs appears

4. View Transactions list
    - A listing of their transactions appears

5. Click New Timelog
    - New timelog form appears, with the person already populated and unchangeable
    - A cancel button appears that takes you back to the person account view
    - Save takes you back to the person account view
    - Related transactions also appear
    - Equity is updated

6. Click New Transaction
    - New transaction form appears, with the person already populated and unchangeable
    - A cancel button appears that takes you back to the person account view
    - Save takes you back to the person account view
    - Equity is updated

Timelog
1. Create a new timelog
    - Person is not pre-populated, and has autocomplete
    - Selecting 'stand time' provides a payment type option
    - Times are prepopulated with 6 and 9 pm
    - times can be cleared
    - Timelog appears on timelogs list in reverse chron. order
    - related transactions appear, with appropriate payment type (for stand time)

2. Edit a timelog
    - Changing a person moves sweat equity charge
    - Changing a person moves sweat equity credit
    - Changing a time updates transaction quantity
    - Changing a time updates person balance
    - Edit screen does not allow changing payment type
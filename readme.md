#Broke Spoke Front Desk App

##Intro

This app was written several years ago in Meteor.js v1.2. It has not been regularly maintained. This was my first and only Meteor project. We wanted the reactivity, and Meteor was a fast way to get that before the current frontend js frameworks were viable. The upshot was that I was stuck with Mongo when I really wanted a RDBS. 

##Models
My solution to being stuck with Mongo was a lot of related document caching using mongo subdocuments. If I recall, this also simplified things with Meteor's 'minimongo'. Anyway, you can see this e.g. in `models/Timelogs.js`, where there is a `person` Object, which is a full cached version of the related person object rather than just a reference. It's pretty awful. These caches are updated when the models that include them are created or updated, using `before` and `after` hooks provided by a package. Those hook calls are in the model files.

###People
 `People` are the models that relate to visitors and volunteers. This model also stores volunteer credit totals. App `Users` are distinct from `People`.

###Timelogs
These are records of a sign-in to the shop. Depending on business logic, they can generate transactions.

###Transactions
These can be equity or dollars. Equity can be debit or credit, cash is a debit. Equity transactions increase or decrease a `Person` equity balance.

Cash transactions are only used for repair stand rental, and really only serves to notify the admin interface to charge cash using an unrelated POS.



##Views
There are two UIs for the app, the `admin` and the `login` kiosk.


##Deployment
Currently, this app and the production env is set up to use MUP (Meteor up) v 0.11.4, which has been deprecated. It uses SSH keys, which will need to be updated on the server.







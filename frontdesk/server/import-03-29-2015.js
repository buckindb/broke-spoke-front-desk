import3292015 = [
    {
        "firstName":"Darryl",
        "lastName":"Ables",
        "equity":"-6",
        "firstSeen":"7/30/2013 3:00:00",
        "lastSeen":"7/30/2013 3:00:00"
    },
    {
        "firstName":"Victor",
        "lastName":"Aloisio",
        "equity":"-6",
        "firstSeen":"1/3/2013 2:56:56",
        "lastSeen":"2/24/2013 3:00:00"
    },
    {
        "firstName":"Peter",
        "lastName":"Acton",
        "equity":"250",
        "firstSeen":"10/4/2011 3:00:00",
        "lastSeen":"6/19/2012 3:00:00"
    },
    {
        "firstName":"Doug",
        "lastName":"Adams",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Zack",
        "lastName":"Agerton",
        "equity":"12",
        "firstSeen":"1/6/2015 3:00:00",
        "lastSeen":"1/6/2015 3:00:00"
    },
    {
        "firstName":"Sara",
        "lastName":"Ailshire",
        "equity":"250",
        "firstSeen":"",
        "lastSeen":"8/16/2012 3:00:00"
    },
    {
        "firstName":"Pat",
        "lastName":"Anderson",
        "equity":"32",
        "firstSeen":"5/7/2013 3:00:00",
        "lastSeen":"11/24/2013 3:00:00"
    },
    {
        "firstName":"Noah",
        "lastName":"Arb",
        "equity":"16",
        "firstSeen":"8/12/2012 3:00:00",
        "lastSeen":"8/12/2012 3:00:00"
    },
    {
        "firstName":"Tim",
        "lastName":"Arb",
        "equity":"16",
        "firstSeen":"8/12/2012 3:00:00",
        "lastSeen":"8/12/2012 3:00:00"
    },
    {
        "firstName":"Matt",
        "lastName":"Arnsdorf",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Brandon",
        "lastName":"Ard",
        "equity":"0",
        "firstSeen":"9/12/2013 3:00:00",
        "lastSeen":"5/11/2014 3:00:00"
    },
    {
        "firstName":"Mike",
        "lastName":"Arthur",
        "equity":"72",
        "firstSeen":"",
        "lastSeen":"5/18/2014 3:00:00"
    },
    {
        "firstName":"Daniel",
        "lastName":"Aseel",
        "equity":"229",
        "firstSeen":"12/1/2011 3:00:00",
        "lastSeen":"3/19/2015 3:00:00"
    },
    {
        "firstName":"Logan",
        "lastName":"Auritt",
        "equity":"46",
        "firstSeen":"9/14/2014 3:00:00",
        "lastSeen":"9/23/2014 3:00:00"
    },
    {
        "firstName":"Rickey",
        "lastName":"Baker",
        "equity":"50",
        "firstSeen":"",
        "lastSeen":"8/26/2014 3:00:00"
    },
    {
        "firstName":"Chelsey",
        "lastName":"Barbour",
        "equity":"12",
        "firstSeen":"",
        "lastSeen":"9/10/2013 3:00:00"
    },
    {
        "firstName":"Haley",
        "lastName":"Barlow",
        "equity":"230",
        "firstSeen":"9/11/2011 3:00:00",
        "lastSeen":"7/24/2014 3:00:00"
    },
    {
        "firstName":"Stephen",
        "lastName":"Barrett",
        "equity":"221",
        "firstSeen":"3/27/2014 3:00:00",
        "lastSeen":"7/8/2014 3:00:00"
    },
    {
        "firstName":"Terrence",
        "lastName":"Barrett",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Adam",
        "lastName":"Barnes",
        "equity":"0",
        "firstSeen":"1/5/2014 3:00:00",
        "lastSeen":"2/12/2015 3:00:00"
    },
    {
        "firstName":"Leonard",
        "lastName":"Barnett",
        "equity":"11",
        "firstSeen":"6/23/2013 3:00:00",
        "lastSeen":"10/14/2014 3:00:00"
    },
    {
        "firstName":"Stephen",
        "lastName":"Barrett",
        "equity":"17",
        "firstSeen":"6/23/2013 3:00:00",
        "lastSeen":"7/8/2014 3:00:00"
    },
    {
        "firstName":"L D",
        "lastName":"Barnett",
        "equity":"21",
        "firstSeen":"6/23/2013 3:00:00",
        "lastSeen":"10/13/2013 3:00:00"
    },
    {
        "firstName":"Stanley",
        "lastName":"Baker",
        "equity":"-50",
        "firstSeen":"",
        "lastSeen":"5/2/2013 3:00:00"
    },
    {
        "firstName":"Janell",
        "lastName":"Basso",
        "equity":"62",
        "firstSeen":"",
        "lastSeen":"9/10/2013 3:00:00"
    },
    {
        "firstName":"Robert",
        "lastName":"Baxley",
        "equity":"6",
        "firstSeen":"",
        "lastSeen":"9/23/2014 3:00:00"
    },
    {
        "firstName":"Jack",
        "lastName":"Baugh",
        "equity":"24",
        "firstSeen":"9/3/2014 3:00:00",
        "lastSeen":"9/3/2014 3:00:00"
    },
    {
        "firstName":"William",
        "lastName":"Beasley",
        "equity":"24",
        "firstSeen":"9/3/2014 3:00:00",
        "lastSeen":"9/3/2014 3:00:00"
    },
    {
        "firstName":"Bryan",
        "lastName":"Bentley",
        "equity":"35",
        "firstSeen":"11/20/2014 3:00:00",
        "lastSeen":"12/16/2014 3:00:00"
    },
    {
        "firstName":"Elif",
        "lastName":"Bengu",
        "equity":"250",
        "firstSeen":"7/1/2012 3:00:00",
        "lastSeen":"7/31/2012 3:00:00"
    },
    {
        "firstName":"Larry",
        "lastName":"Benson",
        "equity":"-8",
        "firstSeen":"8/14/2014 3:00:00",
        "lastSeen":"8/14/2014 3:00:00"
    },
    {
        "firstName":"James",
        "lastName":"Bills",
        "equity":"-35",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Fred",
        "lastName":"Birch",
        "equity":"20",
        "firstSeen":"7/28/2013 3:00:00",
        "lastSeen":"8/8/2013 3:00:00"
    },
    {
        "firstName":"Kenneth",
        "lastName":"Birch",
        "equity":"-38",
        "firstSeen":"1/26/2012 3:00:00",
        "lastSeen":"8/19/2014 3:00:00"
    },
    {
        "firstName":"David",
        "lastName":"Blythe",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":"4/17/2014 3:00:00"
    },
    {
        "firstName":"Dylin",
        "lastName":"Blount",
        "equity":"16",
        "firstSeen":"9/10/2013 3:00:00",
        "lastSeen":"9/10/2013 3:00:00"
    },
    {
        "firstName":"Jeff",
        "lastName":"Botkin",
        "equity":"-4",
        "firstSeen":"3/23/2014 3:00:00",
        "lastSeen":"3/23/2014 3:00:00"
    },
    {
        "firstName":"Lance",
        "lastName":"Bowman",
        "equity":"0",
        "firstSeen":"11/5/2013 3:00:00",
        "lastSeen":"12/16/2014 3:00:00"
    },
    {
        "firstName":"Marshall",
        "lastName":"Boggess",
        "equity":"12",
        "firstSeen":"11/4/2012 2:00:00",
        "lastSeen":"11/4/2012 2:00:00"
    },
    {
        "firstName":"Robert",
        "lastName":"Brandon",
        "equity":"8",
        "firstSeen":"",
        "lastSeen":"9/3/2014 3:00:00"
    },
    {
        "firstName":"Danny",
        "lastName":"Brown",
        "equity":"14",
        "firstSeen":"",
        "lastSeen":"8/22/2013 3:00:00"
    },
    {
        "firstName":"Neal",
        "lastName":"Blaine",
        "equity":"-10",
        "firstSeen":"3/19/2013 3:00:00",
        "lastSeen":"3/19/2013 3:00:00"
    },
    {
        "firstName":"Sean",
        "lastName":"Boyd",
        "equity":"0",
        "firstSeen":"2/5/2013 3:00:00",
        "lastSeen":"4/14/2013 3:00:00"
    },
    {
        "firstName":"Jake",
        "lastName":"Boyle",
        "equity":"30",
        "firstSeen":"9/6/2012 3:00:00",
        "lastSeen":"9/31/12"
    },
    {
        "firstName":"Luke",
        "lastName":"Box",
        "equity":"250",
        "firstSeen":"10/4/2011 3:00:00",
        "lastSeen":"1/22/2015 3:00:00"
    },
    {
        "firstName":"Andrew",
        "lastName":"Branson",
        "equity":"38",
        "firstSeen":"8/7/2012 3:00:00",
        "lastSeen":"2/21/2013 3:00:00"
    },
    {
        "firstName":"Jessi",
        "lastName":"Breen",
        "equity":"195",
        "firstSeen":"6/19/2014 3:00:00",
        "lastSeen":"3/15/2015 3:00:00"
    },
    {
        "firstName":"Jarred",
        "lastName":"Brewster",
        "equity":"8",
        "firstSeen":"2/2/2012 3:00:00",
        "lastSeen":"9/20/2012 3:00:00"
    },
    {
        "firstName":"Aaron",
        "lastName":"Brewer",
        "equity":"28",
        "firstSeen":"12/7/2014 3:00:00",
        "lastSeen":"12/7/2014 3:00:00"
    },
    {
        "firstName":"Nathan",
        "lastName":"Briggs",
        "equity":"0",
        "firstSeen":"6/20/2013 3:00:00",
        "lastSeen":"9/30/2014 3:00:00"
    },
    {
        "firstName":"Tim",
        "lastName":"Brock",
        "equity":"-14",
        "firstSeen":"7/10/2012 3:00:00",
        "lastSeen":"11/17/2013 3:00:00"
    },
    {
        "firstName":"Chris",
        "lastName":"Brooks",
        "equity":"16",
        "firstSeen":"9/11/2011 3:00:00",
        "lastSeen":"9/8/2013 3:00:00"
    },
    {
        "firstName":"Cale",
        "lastName":"Brashear",
        "equity":"0",
        "firstSeen":"6/22/2014 3:00:00",
        "lastSeen":"7/13/2014 3:00:00"
    },
    {
        "firstName":"James",
        "lastName":"Brooks",
        "equity":"164",
        "firstSeen":"9/3/2013 3:00:00",
        "lastSeen":"6/15/2014 3:00:00"
    },
    {
        "firstName":"Richard",
        "lastName":"Brooks",
        "equity":"2",
        "firstSeen":"11/4/2012 2:00:00",
        "lastSeen":"12/11/2012 3:00:00"
    },
    {
        "firstName":"Alex",
        "lastName":"Brown",
        "equity":"0",
        "firstSeen":"9/11/2012 3:00:00",
        "lastSeen":"11/4/2012 2:00:00"
    },
    {
        "firstName":"Claire",
        "lastName":"Brown",
        "equity":"0",
        "firstSeen":"10/2/2014 3:00:00",
        "lastSeen":"12/⁷25/11115"
    },
    {
        "firstName":"Jim or James",
        "lastName":"Brown",
        "equity":"23",
        "firstSeen":"",
        "lastSeen":"5/17/2012 3:00:00"
    },
    {
        "firstName":"Robert",
        "lastName":"Brown",
        "equity":"-1",
        "firstSeen":"",
        "lastSeen":"9/8/2013 3:00:00"
    },
    {
        "firstName":"Shannon",
        "lastName":"Brown",
        "equity":"-75",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"5/19/2013 3:00:00"
    },
    {
        "firstName":"undefined",
        "lastName":"Bull Horn",
        "equity":"140",
        "firstSeen":"5/30/2013 3:00:00",
        "lastSeen":"5/30/2013 3:00:00"
    },
    {
        "firstName":"Dustin",
        "lastName":"Burchett",
        "equity":"-26",
        "firstSeen":"8/813",
        "lastSeen":"8/10/2013 3:00:00"
    },
    {
        "firstName":"Kasey",
        "lastName":"Burge",
        "equity":"25",
        "firstSeen":"",
        "lastSeen":"1/20/2015 3:00:00"
    },
    {
        "firstName":"Anthony",
        "lastName":"Burge",
        "equity":"16",
        "firstSeen":"",
        "lastSeen":"10/28/2014 3:00:00"
    },
    {
        "firstName":"Eileen",
        "lastName":"Burk",
        "equity":"250",
        "firstSeen":"6/12/2012 3:00:00",
        "lastSeen":"12/10/2013 3:00:00"
    },
    {
        "firstName":"Nick",
        "lastName":"Buckler",
        "equity":"-19",
        "firstSeen":"7/20/2014 3:00:00",
        "lastSeen":"10/27/2014 3:00:00"
    },
    {
        "firstName":"Kathleen",
        "lastName":"Burke",
        "equity":"16",
        "firstSeen":"9/6/2011 3:00:00",
        "lastSeen":"9/6/2011 3:00:00"
    },
    {
        "firstName":"Albert",
        "lastName":"Burks",
        "equity":"-5",
        "firstSeen":"12/2/2014 3:00:00",
        "lastSeen":"12/2/2014 3:00:00"
    },
    {
        "firstName":"Tony",
        "lastName":"Burnett",
        "equity":"28",
        "firstSeen":"4/7/2013 3:00:00",
        "lastSeen":"4/9/2013 3:00:00"
    },
    {
        "firstName":"Casey (c-bird)",
        "lastName":"Byrd",
        "equity":"37",
        "firstSeen":"8/12/2012 3:00:00",
        "lastSeen":"3/12/2015 3:00:00"
    },
    {
        "firstName":"Warren",
        "lastName":"Byrom",
        "equity":"44",
        "firstSeen":"8/12/2012 3:00:00",
        "lastSeen":"8/12/2012 3:00:00"
    },
    {
        "firstName":"Chris",
        "lastName":"Camugila",
        "equity":"-2",
        "firstSeen":"7/24/2012 3:00:00",
        "lastSeen":"10/21/2012 3:00:00"
    },
    {
        "firstName":"Amy",
        "lastName":"Carroll",
        "equity":"28",
        "firstSeen":"8/4/2013 3:00:00",
        "lastSeen":"8/4/2013 3:00:00"
    },
    {
        "firstName":"Victor",
        "lastName":"Caskey",
        "equity":"-18",
        "firstSeen":"10/19/2014 3:00:00",
        "lastSeen":"10/19/2014 3:00:00"
    },
    {
        "firstName":"undefined",
        "lastName":"Cates Family",
        "equity":"0",
        "firstSeen":"9/15/2013 3:00:00",
        "lastSeen":"9/17/2013 3:00:00"
    },
    {
        "firstName":"Adrian",
        "lastName":"Dewayne-Carty",
        "equity":"35",
        "firstSeen":"8/8/2013 3:00:00",
        "lastSeen":"4/10/2014 3:00:00"
    },
    {
        "firstName":"Tyson",
        "lastName":"Carroll",
        "equity":"28",
        "firstSeen":"8/4/2013 3:00:00",
        "lastSeen":"8/4/2013 3:00:00"
    },
    {
        "firstName":"Maggie",
        "lastName":"Carter",
        "equity":"32",
        "firstSeen":"10/4/2011 3:00:00",
        "lastSeen":"11/15/2011 3:00:00"
    },
    {
        "firstName":"Doug",
        "lastName":"Cichon",
        "equity":"44",
        "firstSeen":"5/4/2014 3:00:00",
        "lastSeen":"5/13/2014 3:00:00"
    },
    {
        "firstName":"Zak",
        "lastName":"Clare-Salzler",
        "equity":"203",
        "firstSeen":"9/3/2013 3:00:00",
        "lastSeen":"4/15/2014 3:00:00"
    },
    {
        "firstName":"Jack",
        "lastName":"Clarkson",
        "equity":"250",
        "firstSeen":"",
        "lastSeen":"9/28/2014 3:00:00"
    },
    {
        "firstName":"Robert",
        "lastName":"Claypool",
        "equity":"0",
        "firstSeen":"2/25/2015 3:00:00",
        "lastSeen":"2/2/6/15"
    },
    {
        "firstName":"Anna",
        "lastName":"Clements",
        "equity":"16",
        "firstSeen":"1/19/2012 3:00:00",
        "lastSeen":"1/19/2012 3:00:00"
    },
    {
        "firstName":"Greg",
        "lastName":"Capillo",
        "equity":"0",
        "firstSeen":"7/30/2013 3:00:00",
        "lastSeen":"7/30/2013 3:00:00"
    },
    {
        "firstName":"Ben",
        "lastName":"Cleinmark",
        "equity":"16",
        "firstSeen":"9/9/2014 3:00:00",
        "lastSeen":"9/9/2014 3:00:00"
    },
    {
        "firstName":"Joseph",
        "lastName":"Cobb",
        "equity":"95",
        "firstSeen":"5/9/2013 3:00:00",
        "lastSeen":"6/30/2013 3:00:00"
    },
    {
        "firstName":"Daniel",
        "lastName":"Cockoyne",
        "equity":"-59",
        "firstSeen":"6/19/2014 3:00:00",
        "lastSeen":"6/19/2014 3:00:00"
    },
    {
        "firstName":"Bill",
        "lastName":"Cole",
        "equity":"26",
        "firstSeen":"11/17/2011 3:00:00",
        "lastSeen":"2/27/2014 3:00:00"
    },
    {
        "firstName":"Chris",
        "lastName":"Cole",
        "equity":"0",
        "firstSeen":"2/3/2013 3:00:00",
        "lastSeen":"3/13/2014 3:00:00"
    },
    {
        "firstName":"Christine",
        "lastName":"Cole",
        "equity":"49",
        "firstSeen":"2/26/2013 3:00:00",
        "lastSeen":"2/27/2014 3:00:00"
    },
    {
        "firstName":"Robert",
        "lastName":"Collins",
        "equity":"-38",
        "firstSeen":"11/11/2012 3:00:00",
        "lastSeen":"7/11/2013 3:00:00"
    },
    {
        "firstName":"Patrick",
        "lastName":"Colwell",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Asher",
        "lastName":"Combs",
        "equity":"40",
        "firstSeen":"12/15/2011 3:00:00",
        "lastSeen":"12/17/2014 3:00:00"
    },
    {
        "firstName":"Colin",
        "lastName":"Cook",
        "equity":"-75",
        "firstSeen":"8/9/2012 3:00:00",
        "lastSeen":"8/9/2012 3:00:00"
    },
    {
        "firstName":"Sam",
        "lastName":"Conder",
        "equity":"250",
        "firstSeen":"10/18/2012 3:00:00",
        "lastSeen":"6/6/2013 3:00:00"
    },
    {
        "firstName":"Nick",
        "lastName":"Cooney",
        "equity":"112",
        "firstSeen":"11/13/2011 3:00:00",
        "lastSeen":"10/21/2012 3:00:00"
    },
    {
        "firstName":"Kyle",
        "lastName":"Conley",
        "equity":"0",
        "firstSeen":"5/12/2013 3:00:00",
        "lastSeen":"5/12/2013 3:00:00"
    },
    {
        "firstName":"Phillip",
        "lastName":"Conley",
        "equity":"80",
        "firstSeen":"8/22/2013 3:00:00",
        "lastSeen":"8/29/2013 3:00:00"
    },
    {
        "firstName":"Steve",
        "lastName":"Cox",
        "equity":"0",
        "firstSeen":"5/21/2013 3:00:00",
        "lastSeen":"5/21/2013 3:00:00"
    },
    {
        "firstName":"Dave",
        "lastName":"Cooper",
        "equity":"44",
        "firstSeen":"9/4/2012 3:00:00",
        "lastSeen":"9/18/2012 3:00:00"
    },
    {
        "firstName":"Robert",
        "lastName":"Cornell",
        "equity":"100",
        "firstSeen":"1/15/2015 3:00:00",
        "lastSeen":"3/18/2015 3:00:00"
    },
    {
        "firstName":"Steven",
        "lastName":"Curtis",
        "equity":"250",
        "firstSeen":"10/9/2011 3:00:00",
        "lastSeen":"3/19/2015 3:00:00"
    },
    {
        "firstName":"Tracy",
        "lastName":"Curtis",
        "equity":"90",
        "firstSeen":"10/9/2011 3:00:00",
        "lastSeen":"7/25/2013 3:00:00"
    },
    {
        "firstName":"Tray",
        "lastName":"Curtis",
        "equity":"45",
        "firstSeen":"10/9/2011 3:00:00",
        "lastSeen":"3/19/2015 3:00:00"
    },
    {
        "firstName":"Doug",
        "lastName":"Curl",
        "equity":"30",
        "firstSeen":"7/26/2012 3:00:00",
        "lastSeen":"8/12/2012 3:00:00"
    },
    {
        "firstName":"Maurice",
        "lastName":"Curry",
        "equity":"0",
        "firstSeen":"8/21/2012 3:00:00",
        "lastSeen":"8/26/2012 3:00:00"
    },
    {
        "firstName":"Jonathan",
        "lastName":"D'Emidio",
        "equity":"2",
        "firstSeen":"7/23/2013 3:00:00",
        "lastSeen":"3/27/2014 3:00:00"
    },
    {
        "firstName":"Andrew",
        "lastName":"Dale",
        "equity":"28",
        "firstSeen":"5/5/2013 3:00:00",
        "lastSeen":"3/19/2015 3:00:00"
    },
    {
        "firstName":"James (Jimmy)",
        "lastName":"Dale",
        "equity":"-25",
        "firstSeen":"10/28/2012 3:00:00",
        "lastSeen":"10/28/2012 3:00:00"
    },
    {
        "firstName":"Richard",
        "lastName":"Dalton",
        "equity":"198",
        "firstSeen":"9/28/2014 3:00:00",
        "lastSeen":"3/25/2015 3:00:00"
    },
    {
        "firstName":"Sean",
        "lastName":"Darrow",
        "equity":"20",
        "firstSeen":"2/4/2015 3:00:00",
        "lastSeen":"2/25/2015 3:00:00"
    },
    {
        "firstName":"Brooke",
        "lastName":"Davies",
        "equity":"16",
        "firstSeen":"",
        "lastSeen":"3/4/2012 3:00:00"
    },
    {
        "firstName":"Rain",
        "lastName":"Davis",
        "equity":"4",
        "firstSeen":"1/20/2013 3:00:00",
        "lastSeen":"9/3/2013 3:00:00"
    },
    {
        "firstName":"Tamara",
        "lastName":"Davis",
        "equity":"0",
        "firstSeen":"6/13/2013 3:00:00",
        "lastSeen":"6/13/2013 3:00:00"
    },
    {
        "firstName":"Andrew",
        "lastName":"Deane",
        "equity":"115",
        "firstSeen":"9/17/2014 3:00:00",
        "lastSeen":"3/19/2015 3:00:00"
    },
    {
        "firstName":"George",
        "lastName":"DeLullo",
        "equity":"56",
        "firstSeen":"10/19/2014 3:00:00",
        "lastSeen":"11/16/2014 3:00:00"
    },
    {
        "firstName":"Juan",
        "lastName":"Diaz",
        "equity":"154",
        "firstSeen":"7/7/2013 3:00:00",
        "lastSeen":"9/4/2014 3:00:00"
    },
    {
        "firstName":"Sam",
        "lastName":"Dick",
        "equity":"24",
        "firstSeen":"",
        "lastSeen":"3/18/2012 3:00:00"
    },
    {
        "firstName":"David",
        "lastName":"Dickason",
        "equity":"186",
        "firstSeen":"",
        "lastSeen":"4/17/2014 3:00:00"
    },
    {
        "firstName":"Michael",
        "lastName":"Dismuke",
        "equity":"33",
        "firstSeen":"10/10/2013 3:00:00",
        "lastSeen":"1/22/2014 3:00:00"
    },
    {
        "firstName":"Ronald",
        "lastName":"Denny",
        "equity":"-75",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"5/19/2013 3:00:00"
    },
    {
        "firstName":"James",
        "lastName":"Denny",
        "equity":"3",
        "firstSeen":"8/5/2014 3:00:00",
        "lastSeen":"9/28/2014 3:00:00"
    },
    {
        "firstName":"Leah",
        "lastName":"Dodge",
        "equity":"10",
        "firstSeen":"",
        "lastSeen":"5/3/2012 3:00:00"
    },
    {
        "firstName":"Ernie",
        "lastName":"Dominguez",
        "equity":"342",
        "firstSeen":"7/1/2012 3:00:00",
        "lastSeen":"10/14/2014 3:00:00"
    },
    {
        "firstName":"David",
        "lastName":"Downing",
        "equity":"44",
        "firstSeen":"1/22/2012 3:00:00",
        "lastSeen":"1/29/2012 3:00:00"
    },
    {
        "firstName":"Odi",
        "lastName":"Drake",
        "equity":"-14",
        "firstSeen":"11/10/2011 3:00:00",
        "lastSeen":"11/13/2011 3:00:00"
    },
    {
        "firstName":"Steve",
        "lastName":"Driscoll",
        "equity":"8",
        "firstSeen":"11/20/2012 3:00:00",
        "lastSeen":"11/20/2012 3:00:00"
    },
    {
        "firstName":"Adam",
        "lastName":"Drye",
        "equity":"0",
        "firstSeen":"6/17/2012 3:00:00",
        "lastSeen":"6/17/2012 3:00:00"
    },
    {
        "firstName":"Andy",
        "lastName":"Duckworth",
        "equity":"207",
        "firstSeen":"multiple",
        "lastSeen":"3/8/2015 4:00:00"
    },
    {
        "firstName":"Moses",
        "lastName":"Duran",
        "equity":"71",
        "firstSeen":"8/12/2014 3:00:00",
        "lastSeen":"10/29/2014 3:00:00"
    },
    {
        "firstName":"Duke",
        "lastName":"Dunn",
        "equity":"24",
        "firstSeen":"7/9/2013 3:00:00",
        "lastSeen":"7/9/2013 3:00:00"
    },
    {
        "firstName":"Alan",
        "lastName":"Dunham",
        "equity":"100",
        "firstSeen":"2/4/2015 3:00:00",
        "lastSeen":"3/18/2015 3:00:00"
    },
    {
        "firstName":"Blake",
        "lastName":"Durham",
        "equity":"22",
        "firstSeen":"11/13/2011 3:00:00",
        "lastSeen":"11/10/2011 3:00:00"
    },
    {
        "firstName":"Ian",
        "lastName":"Dutton",
        "equity":"-14",
        "firstSeen":"9/30/2012 3:00:00",
        "lastSeen":"10/14/2012 3:00:00"
    },
    {
        "firstName":"Paul",
        "lastName":"Eldred",
        "equity":"20",
        "firstSeen":"9/30/2012 3:00:00",
        "lastSeen":"10/14/2012 3:00:00"
    },
    {
        "firstName":"Cortez",
        "lastName":"Edwards",
        "equity":"9",
        "firstSeen":"9/16/2014 3:00:00",
        "lastSeen":"9/16/2014 3:00:00"
    },
    {
        "firstName":"Stephen",
        "lastName":"Eidson",
        "equity":"16",
        "firstSeen":"6/22/2014 3:00:00",
        "lastSeen":"6/22/2014 3:00:00"
    },
    {
        "firstName":"Brad",
        "lastName":"Finnera",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Jimmy",
        "lastName":"Finley",
        "equity":"7",
        "firstSeen":"11/13/2011 3:00:00",
        "lastSeen":"11/13/2011 3:00:00"
    },
    {
        "firstName":"Matthew",
        "lastName":"Fletcher",
        "equity":"-27",
        "firstSeen":"8/7/2014 3:00:00",
        "lastSeen":"10/7/2014 3:00:00"
    },
    {
        "firstName":"Brent",
        "lastName":"Flores",
        "equity":"16",
        "firstSeen":"7/14/2013 3:00:00",
        "lastSeen":"7/14/2013 3:00:00"
    },
    {
        "firstName":"Brice",
        "lastName":"Followell",
        "equity":"14",
        "firstSeen":"6/11/2013 3:00:00",
        "lastSeen":"6/18/2013 3:00:00"
    },
    {
        "firstName":"Tessa",
        "lastName":"Folsom",
        "equity":"4",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Thomas",
        "lastName":"Forga",
        "equity":"78",
        "firstSeen":"9/11/2014 3:00:00",
        "lastSeen":"2/22/2015 3:00:00"
    },
    {
        "firstName":"Josh",
        "lastName":"Frederick",
        "equity":"0",
        "firstSeen":"9/25/2011 3:00:00",
        "lastSeen":"9/25/2011 3:00:00"
    },
    {
        "firstName":"Jed",
        "lastName":"Frey",
        "equity":"2",
        "firstSeen":"9/11/2012 3:00:00",
        "lastSeen":"9/11/2012 3:00:00"
    },
    {
        "firstName":"Mike",
        "lastName":"Friedman",
        "equity":"34",
        "firstSeen":"12/6/2013 3:00:00",
        "lastSeen":"12/12/2013 3:00:00"
    },
    {
        "firstName":"James",
        "lastName":"Fromen",
        "equity":"-5",
        "firstSeen":"",
        "lastSeen":"9/28/2014 3:00:00"
    },
    {
        "firstName":"Anthony",
        "lastName":"Ferguson",
        "equity":"0",
        "firstSeen":"3/21/2013 3:00:00",
        "lastSeen":"3/21/2013 3:00:00"
    },
    {
        "firstName":"John",
        "lastName":"Gaines",
        "equity":"0",
        "firstSeen":"8/24/2014 3:00:00",
        "lastSeen":"8/24/2014 3:00:00"
    },
    {
        "firstName":"Brian",
        "lastName":"Garcia",
        "equity":"56",
        "firstSeen":"11/27/2011 3:00:00",
        "lastSeen":"3/4/2012 3:00:00"
    },
    {
        "firstName":"Mary",
        "lastName":"Gee",
        "equity":"64",
        "firstSeen":"11/10/2011 3:00:00",
        "lastSeen":"11/10/2013 3:00:00"
    },
    {
        "firstName":"Christian",
        "lastName":"Gies",
        "equity":"6",
        "firstSeen":"",
        "lastSeen":"1/17/2013 3:00:00"
    },
    {
        "firstName":"Rebecca",
        "lastName":"Gladding",
        "equity":"2",
        "firstSeen":"4/2/2013 3:00:00",
        "lastSeen":"5/2/2013 3:00:00"
    },
    {
        "firstName":"Luke",
        "lastName":"Gnadinger",
        "equity":"20",
        "firstSeen":"9/25/2011 3:00:00",
        "lastSeen":"10/10/2013 3:00:00"
    },
    {
        "firstName":"Briana",
        "lastName":"Godfrey",
        "equity":"0",
        "firstSeen":"9/25/2011 3:00:00",
        "lastSeen":"9/25/2011 3:00:00"
    },
    {
        "firstName":"Will",
        "lastName":"Goodman",
        "equity":"50",
        "firstSeen":"6/2/2013 3:00:00",
        "lastSeen":"6/2/2013 3:00:00"
    },
    {
        "firstName":"James",
        "lastName":"Gonyer",
        "equity":"21",
        "firstSeen":"",
        "lastSeen":"9/3/2014 3:00:00"
    },
    {
        "firstName":"Stephen",
        "lastName":"Gordinier",
        "equity":"-20",
        "firstSeen":"9/6/2012 3:00:00",
        "lastSeen":"5/9/2013 3:00:00"
    },
    {
        "firstName":"Daniel",
        "lastName":"Gray",
        "equity":"22",
        "firstSeen":"4/28/2013 3:00:00",
        "lastSeen":"4/16/2013 3:00:00"
    },
    {
        "firstName":"Granola",
        "lastName":"Granola",
        "equity":"20",
        "firstSeen":"8/12/2014 3:00:00",
        "lastSeen":"8/12/2014 3:00:00"
    },
    {
        "firstName":"Robert",
        "lastName":"Grimes",
        "equity":"14",
        "firstSeen":"",
        "lastSeen":"6/9/2013 3:00:00"
    },
    {
        "firstName":"Donde",
        "lastName":"Green",
        "equity":"20",
        "firstSeen":"",
        "lastSeen":"11/21/2013 3:00:00"
    },
    {
        "firstName":"Kayla",
        "lastName":"Greer",
        "equity":"12",
        "firstSeen":"",
        "lastSeen":"3/18/2015 3:00:00"
    },
    {
        "firstName":"Seth",
        "lastName":"Greer",
        "equity":"60",
        "firstSeen":"",
        "lastSeen":"3/25/2015 3:00:00"
    },
    {
        "firstName":"Shannon",
        "lastName":"Greer",
        "equity":"24",
        "firstSeen":"",
        "lastSeen":"8/14/2014 3:00:00"
    },
    {
        "firstName":"Richard",
        "lastName":"Grigsby",
        "equity":"16",
        "firstSeen":"",
        "lastSeen":"8/14/2014 3:00:00"
    },
    {
        "firstName":"Jeff",
        "lastName":"Grover",
        "equity":"16",
        "firstSeen":"",
        "lastSeen":"3/4/2012 3:00:00"
    },
    {
        "firstName":"Yigit",
        "lastName":"Guvercin",
        "equity":"144",
        "firstSeen":"2/11/2015 3:00:00",
        "lastSeen":"3/22/2015 3:00:00"
    },
    {
        "firstName":"Andre",
        "lastName":"Guy",
        "equity":"29",
        "firstSeen":"7/8/2012 3:00:00",
        "lastSeen":"7/12/2012 3:00:00"
    },
    {
        "firstName":"Coleman",
        "lastName":"Guyon",
        "equity":"175",
        "firstSeen":"10/23/2012 3:00:00",
        "lastSeen":"12/6/2012 3:00:00"
    },
    {
        "firstName":"Shane",
        "lastName":"Haezebrouck",
        "equity":"28",
        "firstSeen":"10/21/2012 3:00:00",
        "lastSeen":"10/21/2012 3:00:00"
    },
    {
        "firstName":"Tom",
        "lastName":"Haezebrouck",
        "equity":"0",
        "firstSeen":"10/21/2012 3:00:00",
        "lastSeen":"12/20/2012 3:00:00"
    },
    {
        "firstName":"Dustin",
        "lastName":"Haider",
        "equity":"-11",
        "firstSeen":"7/26/2012 3:00:00",
        "lastSeen":"8/2/2012 3:00:00"
    },
    {
        "firstName":"Thomas",
        "lastName":"Hale",
        "equity":"0",
        "firstSeen":"1/22/2012 3:00:00",
        "lastSeen":"1/10/2013 3:00:00"
    },
    {
        "firstName":"Danny",
        "lastName":"Hall",
        "equity":"-75",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"5/19/2013 3:00:00"
    },
    {
        "firstName":"Darrell",
        "lastName":"Hall II",
        "equity":"-20",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Rob",
        "lastName":"Hall",
        "equity":"-42",
        "firstSeen":"11/11/2012 3:00:00",
        "lastSeen":"3/7/2013 3:00:00"
    },
    {
        "firstName":"Carlos",
        "lastName":"Hagan",
        "equity":"8",
        "firstSeen":"12/12/2013 3:00:00",
        "lastSeen":"12/12/2013 3:00:00"
    },
    {
        "firstName":"Tyjuan",
        "lastName":"Hagan",
        "equity":"4",
        "firstSeen":"",
        "lastSeen":"12/21/2014 3:00:00"
    },
    {
        "firstName":"Ken",
        "lastName":"Hagele",
        "equity":"56",
        "firstSeen":"8/3/2014 3:00:00",
        "lastSeen":"8/3/2014 3:00:00"
    },
    {
        "firstName":"Walter",
        "lastName":"Hamilton",
        "equity":"-14",
        "firstSeen":"3/17/2013 3:00:00",
        "lastSeen":"3/19/2013 3:00:00"
    },
    {
        "firstName":"Cameron",
        "lastName":"Hamilton",
        "equity":"34",
        "firstSeen":"2/4/2015 3:00:00",
        "lastSeen":"3/18/2015 3:00:00"
    },
    {
        "firstName":"Jonathan",
        "lastName":"Hamilton",
        "equity":"0",
        "firstSeen":"6/23/2013 3:00:00",
        "lastSeen":"6/23/2013 3:00:00"
    },
    {
        "firstName":"Robert",
        "lastName":"Hamilton",
        "equity":"-18",
        "firstSeen":"5/21/2013 3:00:00",
        "lastSeen":"6/2/2013 3:00:00"
    },
    {
        "firstName":"Bruce",
        "lastName":"Hammond",
        "equity":"10",
        "firstSeen":"9/20/2012 3:00:00",
        "lastSeen":"1/29/2013 3:00:00"
    },
    {
        "firstName":"richard",
        "lastName":"Hammons",
        "equity":"-6",
        "firstSeen":"11/14/2013 3:00:00",
        "lastSeen":"11/14/2013 3:00:00"
    },
    {
        "firstName":"Scott",
        "lastName":"Hannigan",
        "equity":"0",
        "firstSeen":"8/14/2012 3:00:00",
        "lastSeen":"8/14/2012 3:00:00"
    },
    {
        "firstName":"Kyle",
        "lastName":"Hard",
        "equity":"16",
        "firstSeen":"1/19/2104 2:56:56",
        "lastSeen":"1/19/2104 2:56:56"
    },
    {
        "firstName":"Matt",
        "lastName":"Hardin",
        "equity":"-68",
        "firstSeen":"5/1/2013 3:00:00",
        "lastSeen":"5/1/2013 3:00:00"
    },
    {
        "firstName":"Calvin",
        "lastName":"Harmin",
        "equity":"0",
        "firstSeen":"9/11/2012 3:00:00",
        "lastSeen":"9/23/2012 3:00:00"
    },
    {
        "firstName":"Timothy",
        "lastName":"Hatton",
        "equity":"9",
        "firstSeen":"",
        "lastSeen":"5/1/2014 3:00:00"
    },
    {
        "firstName":"Steven",
        "lastName":"Hawkins",
        "equity":"-42",
        "firstSeen":"7/15/2014 3:00:00",
        "lastSeen":"7/17/2014 3:00:00"
    },
    {
        "firstName":"Therria",
        "lastName":"Hayes",
        "equity":"46",
        "firstSeen":"7/21/2013 3:00:00",
        "lastSeen":"9/23/2014 3:00:00"
    },
    {
        "firstName":"Jeffrey",
        "lastName":"Hayes",
        "equity":"-4",
        "firstSeen":"7/30/2013 3:00:00",
        "lastSeen":"7/30/2013 3:00:00"
    },
    {
        "firstName":"Sara",
        "lastName":"Head",
        "equity":"250",
        "firstSeen":"7/5/2011 3:00:00",
        "lastSeen":"5/8/2012 3:00:00"
    },
    {
        "firstName":"Jeffery",
        "lastName":"Headley",
        "equity":"250",
        "firstSeen":"7/5/2011 3:00:00",
        "lastSeen":"9/2/2014 3:00:00"
    },
    {
        "firstName":"Chris",
        "lastName":"Hemersty",
        "equity":"16",
        "firstSeen":"2/26/2013 3:00:00",
        "lastSeen":"2/26/2013 3:00:00"
    },
    {
        "firstName":"David",
        "lastName":"Hempy",
        "equity":"0",
        "firstSeen":"2/26/2013 3:00:00",
        "lastSeen":"9/10/2013 3:00:00"
    },
    {
        "firstName":"James",
        "lastName":"Henderson",
        "equity":"108",
        "firstSeen":"9/20/2011 3:00:00",
        "lastSeen":"10/21/2014 3:00:00"
    },
    {
        "firstName":"Claire",
        "lastName":"Henkel",
        "equity":"-60",
        "firstSeen":"6/19/2012 3:00:00",
        "lastSeen":"6/19/2012 3:00:00"
    },
    {
        "firstName":"Alex",
        "lastName":"Hessler",
        "equity":"10",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Giovanni",
        "lastName":"Hernandez",
        "equity":"58",
        "firstSeen":"",
        "lastSeen":"3/18/2015 3:00:00"
    },
    {
        "firstName":"Oliver & Kat",
        "lastName":"Hidalgo",
        "equity":"9",
        "firstSeen":"10/1/2013 3:00:00",
        "lastSeen":"10/22/2013 3:00:00"
    },
    {
        "firstName":"Chad",
        "lastName":"Hilton",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"James",
        "lastName":"Hilderbrand",
        "equity":"66",
        "firstSeen":"5/28/2013 3:00:00",
        "lastSeen":"9/8/2013 3:00:00"
    },
    {
        "firstName":"Karla",
        "lastName":"Hines",
        "equity":"22",
        "firstSeen":"11/10/2013 3:00:00",
        "lastSeen":"7/31/2014 3:00:00"
    },
    {
        "firstName":"Jackson",
        "lastName":"Hinton",
        "equity":"30",
        "firstSeen":"10/7/2012 3:00:00",
        "lastSeen":"10/7/2012 3:00:00"
    },
    {
        "firstName":"Mike",
        "lastName":"Hinton",
        "equity":"30",
        "firstSeen":"10/7/2012 3:00:00",
        "lastSeen":"10/7/2012 3:00:00"
    },
    {
        "firstName":"Daniel",
        "lastName":"Hintze",
        "equity":"94",
        "firstSeen":"11/6/2013 3:00:00",
        "lastSeen":"6/3/2014 3:00:00"
    },
    {
        "firstName":"Kevin",
        "lastName":"Hogan",
        "equity":"77",
        "firstSeen":"5/4/2014 3:00:00",
        "lastSeen":"6/1/2014 3:00:00"
    },
    {
        "firstName":"Kevin",
        "lastName":"Holcomb",
        "equity":"-75",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Timothy",
        "lastName":"Holton",
        "equity":"-125",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Nathan",
        "lastName":"Hopper",
        "equity":"-100",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Jacob",
        "lastName":"Hopkins",
        "equity":"-50",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"5/19/2013 3:00:00"
    },
    {
        "firstName":"Tony",
        "lastName":"Horner",
        "equity":"189",
        "firstSeen":"7/13/2014 3:00:00",
        "lastSeen":"3/19/2015 3:00:00"
    },
    {
        "firstName":"Dwight",
        "lastName":"Houck",
        "equity":"32",
        "firstSeen":"6/9/2013 3:00:00",
        "lastSeen":"6/25/2013 3:00:00"
    },
    {
        "firstName":"Shelly",
        "lastName":"Houck",
        "equity":"62",
        "firstSeen":"6/9/2013 3:00:00",
        "lastSeen":"6/25/2013 3:00:00"
    },
    {
        "firstName":"Gary",
        "lastName":"Huff",
        "equity":"250",
        "firstSeen":"11/1/2012 3:00:00",
        "lastSeen":"2/26/2015 3:00:00"
    },
    {
        "firstName":"Hernry",
        "lastName":"Huffines",
        "equity":"8",
        "firstSeen":"12/14/2014 3:00:00",
        "lastSeen":"12/14/2014 3:00:00"
    },
    {
        "firstName":"Mike",
        "lastName":"Huffman",
        "equity":"0",
        "firstSeen":"7/7/2013 3:00:00",
        "lastSeen":"7/14/2013 3:00:00"
    },
    {
        "firstName":"Jason",
        "lastName":"Hughes",
        "equity":"-50",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Justin",
        "lastName":"Hulett",
        "equity":"0",
        "firstSeen":"10/28/2014 3:00:00",
        "lastSeen":"10/28/2014 3:00:00"
    },
    {
        "firstName":"Jacoby",
        "lastName":"Hunt",
        "equity":"18",
        "firstSeen":"",
        "lastSeen":"9/14/2014 3:00:00"
    },
    {
        "firstName":"Mitchell",
        "lastName":"Iles",
        "equity":"-50",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"5/19/2013 3:00:00"
    },
    {
        "firstName":"Matt",
        "lastName":"Ingram",
        "equity":"-75",
        "firstSeen":"8/9/2012 3:00:00",
        "lastSeen":"8/9/2012 3:00:00"
    },
    {
        "firstName":"Margaret",
        "lastName":"Ingram",
        "equity":"-75",
        "firstSeen":"8/9/2012 3:00:00",
        "lastSeen":"8/9/2012 3:00:00"
    },
    {
        "firstName":"Kudra",
        "lastName":"Irakoze",
        "equity":"8",
        "firstSeen":"5/20/2014 3:00:00",
        "lastSeen":"5/20/2014 3:00:00"
    },
    {
        "firstName":"Andy",
        "lastName":"Jackson",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Wayne",
        "lastName":"Jackson",
        "equity":"53",
        "firstSeen":"7/28/2013 3:00:00",
        "lastSeen":"3/22/2015 3:00:00"
    },
    {
        "firstName":"Cale",
        "lastName":"Jacobs",
        "equity":"34",
        "firstSeen":"6/13/2013 3:00:00",
        "lastSeen":"6/27/2013 3:00:00"
    },
    {
        "firstName":"Wayne",
        "lastName":"Jacobson",
        "equity":"20",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Rhianna",
        "lastName":"James",
        "equity":"32",
        "firstSeen":"9/23/2012 3:00:00",
        "lastSeen":"1/29/2013 3:00:00"
    },
    {
        "firstName":"Ed",
        "lastName":"Jennings",
        "equity":"24",
        "firstSeen":"3/28/2013 3:00:00",
        "lastSeen":"3/28/2013 3:00:00"
    },
    {
        "firstName":"Kjre'",
        "lastName":"Jenkins",
        "equity":"10",
        "firstSeen":"",
        "lastSeen":"8/7/2014 3:00:00"
    },
    {
        "firstName":"Bryan",
        "lastName":"Johnson",
        "equity":"24",
        "firstSeen":"8/7/2014 3:00:00",
        "lastSeen":"8/7/2014 3:00:00"
    },
    {
        "firstName":"Leif",
        "lastName":"Johnson",
        "equity":"32",
        "firstSeen":"3/1/2015 3:00:00",
        "lastSeen":"3/1/2015 3:00:00"
    },
    {
        "firstName":"Randy",
        "lastName":"Johnson",
        "equity":"24",
        "firstSeen":"9/9/2014 3:00:00",
        "lastSeen":"9/9/2014 3:00:00"
    },
    {
        "firstName":"Adam",
        "lastName":"Jonas",
        "equity":"24",
        "firstSeen":"4/2/2013 3:00:00",
        "lastSeen":"4/2/2013 3:00:00"
    },
    {
        "firstName":"Daniel",
        "lastName":"Jones",
        "equity":"24",
        "firstSeen":"8/24/2014 3:00:00",
        "lastSeen":"8/24/2014 3:00:00"
    },
    {
        "firstName":"Paul",
        "lastName":"Jones",
        "equity":"24",
        "firstSeen":"6/9/2013 3:00:00",
        "lastSeen":"6/9/2013 3:00:00"
    },
    {
        "firstName":"Paul H.",
        "lastName":"Jones",
        "equity":"100",
        "firstSeen":"6/23/2012 3:00:00",
        "lastSeen":"6/23/2012 3:00:00"
    },
    {
        "firstName":"Amy",
        "lastName":"Jones",
        "equity":"44",
        "firstSeen":"11/13/2012 3:00:00",
        "lastSeen":"11/13/2012 3:00:00"
    },
    {
        "firstName":"Micah",
        "lastName":"Jones",
        "equity":"28",
        "firstSeen":"12/3/2013 3:00:00",
        "lastSeen":"7/15/2014 3:00:00"
    },
    {
        "firstName":"Autumn",
        "lastName":"Joslin",
        "equity":"24",
        "firstSeen":"5/28/2013 3:00:00",
        "lastSeen":"5/28/2013 3:00:00"
    },
    {
        "firstName":"Kelly",
        "lastName":"Jones",
        "equity":"187",
        "firstSeen":"as of 10/18",
        "lastSeen":"11/13/2011 3:00:00"
    },
    {
        "firstName":"Burton",
        "lastName":"Joyner",
        "equity":"100",
        "firstSeen":"12/5/2013 3:00:00",
        "lastSeen":"12/5/2013 3:00:00"
    },
    {
        "firstName":"David",
        "lastName":"Kain",
        "equity":"40",
        "firstSeen":"7/3/2012 3:00:00",
        "lastSeen":"2/8/2015 3:00:00"
    },
    {
        "firstName":"Barry",
        "lastName":"Kidder",
        "equity":"-50",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Jessica",
        "lastName":"Kemmer",
        "equity":"4",
        "firstSeen":"",
        "lastSeen":"8/13/2013 3:00:00"
    },
    {
        "firstName":"Jim",
        "lastName":"Kemper",
        "equity":"20",
        "firstSeen":"3/11/2015 3:00:00",
        "lastSeen":"3/11/2015 3:00:00"
    },
    {
        "firstName":"Ronnie",
        "lastName":"Kennedy",
        "equity":"8",
        "firstSeen":"2/25/2015 3:00:00",
        "lastSeen":"3/25/2015 3:00:00"
    },
    {
        "firstName":"Thomas",
        "lastName":"Kerr",
        "equity":"234",
        "firstSeen":"7/10/2012 3:00:00",
        "lastSeen":"8/28/2012 3:00:00"
    },
    {
        "firstName":"Steven",
        "lastName":"Kimerly",
        "equity":"-20",
        "firstSeen":"10/15/2013 3:00:00",
        "lastSeen":"5/6/2014 3:00:00"
    },
    {
        "firstName":"Johnny",
        "lastName":"King",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":"5/23/2013 3:00:00"
    },
    {
        "firstName":"Terrell",
        "lastName":"King",
        "equity":"-12",
        "firstSeen":"3/30/2014 3:00:00",
        "lastSeen":"3/30/2014 3:00:00"
    },
    {
        "firstName":"Carrie",
        "lastName":"Kirkpatrick",
        "equity":"206",
        "firstSeen":"11/17/2011 3:00:00",
        "lastSeen":"9/4/2012 3:00:00"
    },
    {
        "firstName":"Allen",
        "lastName":"Kirkwood",
        "equity":"295",
        "firstSeen":"",
        "lastSeen":"10/23/2014 3:00:00"
    },
    {
        "firstName":"John",
        "lastName":"Klus",
        "equity":"192",
        "firstSeen":"6/10/2012 3:00:00",
        "lastSeen":"3/18/2015 3:00:00"
    },
    {
        "firstName":"Bruce",
        "lastName":"Klockars",
        "equity":"266",
        "firstSeen":"4/7/2013 3:00:00",
        "lastSeen":"3/18/2015 3:00:00"
    },
    {
        "firstName":"undefined",
        "lastName":"Sheet340",
        "equity":"undefined",
        "firstSeen":"",
        "lastSeen":"undefined"
    },
    {
        "firstName":"Jesse",
        "lastName":"Klus",
        "equity":"46",
        "firstSeen":"6/10/2012 3:00:00",
        "lastSeen":"7/16/2012 3:00:00"
    },
    {
        "firstName":"Jennifer n Cole",
        "lastName":"Knight",
        "equity":"3",
        "firstSeen":"",
        "lastSeen":"4/29/2014 3:00:00"
    },
    {
        "firstName":"Kristy",
        "lastName":"Koenig",
        "equity":"4",
        "firstSeen":"11/27/2011 3:00:00",
        "lastSeen":"11/27/2011 3:00:00"
    },
    {
        "firstName":"John",
        "lastName":"Krueger",
        "equity":"-6",
        "firstSeen":"10/18/2011 3:00:00",
        "lastSeen":"12/15/2011 3:00:00"
    },
    {
        "firstName":"Charles",
        "lastName":"Kuhl",
        "equity":"-86",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"6/9/2013 3:00:00"
    },
    {
        "firstName":"Jamelyn",
        "lastName":"Kyser",
        "equity":"138",
        "firstSeen":"2/16/2012 3:00:00",
        "lastSeen":"8/23/2012 3:00:00"
    },
    {
        "firstName":"Michele",
        "lastName":"Lacey",
        "equity":"20",
        "firstSeen":"",
        "lastSeen":"5/6/2012 3:00:00"
    },
    {
        "firstName":"undefined",
        "lastName":"Lameck",
        "equity":"139",
        "firstSeen":"",
        "lastSeen":"7/3/2014 3:00:00"
    },
    {
        "firstName":"Doug",
        "lastName":"Lally",
        "equity":"2",
        "firstSeen":"6/17/2012 3:00:00",
        "lastSeen":"7/8/2012 3:00:00"
    },
    {
        "firstName":"Josh",
        "lastName":"Lattuca",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Lewis",
        "lastName":"Landis",
        "equity":"20",
        "firstSeen":"9/9/2012 3:00:00",
        "lastSeen":"9/9/2012 3:00:00"
    },
    {
        "firstName":"Kinzell",
        "lastName":"Lawson",
        "equity":"-7",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"undefined",
        "lastName":"Lawton",
        "equity":"-7",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Steven",
        "lastName":"Lay",
        "equity":"14",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"6/23/2013 3:00:00"
    },
    {
        "firstName":"Brandon",
        "lastName":"Leach",
        "equity":"32",
        "firstSeen":"10/9/2011 3:00:00",
        "lastSeen":"10/9/2011 3:00:00"
    },
    {
        "firstName":"Evan",
        "lastName":"Leach",
        "equity":"-10",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Anthony",
        "lastName":"Leadingham",
        "equity":"-100",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"5/19/2013 3:00:00"
    },
    {
        "firstName":"David",
        "lastName":"Leavell",
        "equity":"13",
        "firstSeen":"5/28/2013 3:00:00",
        "lastSeen":"10/25/2013 3:00:00"
    },
    {
        "firstName":"Tim",
        "lastName":"LeBlanc",
        "equity":"-51",
        "firstSeen":"3/10/2013 4:00:00",
        "lastSeen":"3/10/2013 4:00:00"
    },
    {
        "firstName":"David",
        "lastName":"Lepide",
        "equity":"12",
        "firstSeen":"1/15/2012 3:00:00",
        "lastSeen":"1/15/2012 3:00:00"
    },
    {
        "firstName":"Jackson",
        "lastName":"Lester",
        "equity":"46",
        "firstSeen":"10/8/2013 3:00:00",
        "lastSeen":"10/29/2013 3:00:00"
    },
    {
        "firstName":"Andy",
        "lastName":"Lewis",
        "equity":"64",
        "firstSeen":"11/27/2011 3:00:00",
        "lastSeen":"2/19/2012 3:00:00"
    },
    {
        "firstName":"Jason",
        "lastName":"Lickliter",
        "equity":"-10",
        "firstSeen":"3/30/2014 3:00:00",
        "lastSeen":"3/30/2014 3:00:00"
    },
    {
        "firstName":"Anthony",
        "lastName":"Lewis",
        "equity":"6",
        "firstSeen":"8/8/2013 3:00:00",
        "lastSeen":"11/7/2013 3:00:00"
    },
    {
        "firstName":"Corbin",
        "lastName":"Little",
        "equity":"32",
        "firstSeen":"12/20/2011 3:00:00",
        "lastSeen":"6/3/2014 3:00:00"
    },
    {
        "firstName":"Jess",
        "lastName":"Linz",
        "equity":"25",
        "firstSeen":"8/14/2014 3:00:00",
        "lastSeen":"8/24/2014 3:00:00"
    },
    {
        "firstName":"Mitchell",
        "lastName":"Lisk",
        "equity":"8",
        "firstSeen":"9/11/2012 3:00:00",
        "lastSeen":"9/11/2012 3:00:00"
    },
    {
        "firstName":"Collin",
        "lastName":"Lloyd",
        "equity":"4",
        "firstSeen":"6/23/2013 3:00:00",
        "lastSeen":"10/5/2014 3:00:00"
    },
    {
        "firstName":"Anthony",
        "lastName":"Long",
        "equity":"0",
        "firstSeen":"9/11/2014 3:00:00",
        "lastSeen":"9/11/2014 3:00:00"
    },
    {
        "firstName":"Nadiene",
        "lastName":"Long",
        "equity":"4",
        "firstSeen":"8/7/2014 3:00:00",
        "lastSeen":"8/7/2014 3:00:00"
    },
    {
        "firstName":"Kathryn",
        "lastName":"Lybarger",
        "equity":"28",
        "firstSeen":"2/3/2013 3:00:00",
        "lastSeen":"2/10/2013 3:00:00"
    },
    {
        "firstName":"Casey",
        "lastName":"Lyons",
        "equity":"16",
        "firstSeen":"6/17/2012 3:00:00",
        "lastSeen":"3/22/2015 3:00:00"
    },
    {
        "firstName":"Ramsay",
        "lastName":"Omar",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":"8/21/2012 3:00:00"
    },
    {
        "firstName":"undefined",
        "lastName":"Malik",
        "equity":"0",
        "firstSeen":"?",
        "lastSeen":"7/16/2012 3:00:00"
    },
    {
        "firstName":"Derrick",
        "lastName":"Malone",
        "equity":"23",
        "firstSeen":"",
        "lastSeen":"12/18/2014 3:00:00"
    },
    {
        "firstName":"Omar",
        "lastName":"Mandeel",
        "equity":"13",
        "firstSeen":"",
        "lastSeen":"2/5/2013 3:00:00"
    },
    {
        "firstName":"Denzel",
        "lastName":"Manning",
        "equity":"11",
        "firstSeen":"9/16/2012 3:00:00",
        "lastSeen":"10/10/2013 3:00:00"
    },
    {
        "firstName":"Thomas",
        "lastName":"Mapp",
        "equity":"-50",
        "firstSeen":"5/19/2013 3:00:00",
        "lastSeen":"5/19/2013 3:00:00"
    },
    {
        "firstName":"tony",
        "lastName":"markland",
        "equity":"15",
        "firstSeen":"11/9/2014 3:00:00",
        "lastSeen":"12/11/2014 3:00:00"
    },
    {
        "firstName":"Taylor",
        "lastName":"March",
        "equity":"70",
        "firstSeen":"11/1/2011 3:00:00",
        "lastSeen":"10/23/2012 3:00:00"
    },
    {
        "firstName":"Vanessa",
        "lastName":"Marquez",
        "equity":"41",
        "firstSeen":"",
        "lastSeen":"8/19/2012 3:00:00"
    },
    {
        "firstName":"undefined",
        "lastName":"Dave Martin",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Joseph",
        "lastName":"Marzoni",
        "equity":"44",
        "firstSeen":"",
        "lastSeen":"6/10/2014 3:00:00"
    },
    {
        "firstName":"John",
        "lastName":"Maruski",
        "equity":"-6",
        "firstSeen":"8/29/2013 3:00:00",
        "lastSeen":"8/29/2013 3:00:00"
    },
    {
        "firstName":"Harold",
        "lastName":"Masters",
        "equity":"-6",
        "firstSeen":"6/26/2012 3:00:00",
        "lastSeen":"6/26/2012 3:00:00"
    },
    {
        "firstName":"Billy",
        "lastName":"Mason",
        "equity":"24",
        "firstSeen":"11/4/2014 3:00:00",
        "lastSeen":"11/9/2014 3:00:00"
    },
    {
        "firstName":"Scotty",
        "lastName":"Matlock",
        "equity":"4",
        "firstSeen":"2/7/2013 3:00:00",
        "lastSeen":"2/12/2013 3:00:00"
    },
    {
        "firstName":"Mike",
        "lastName":"Maynahan",
        "equity":"8",
        "firstSeen":"",
        "lastSeen":"7/1/2012 3:00:00"
    },
    {
        "firstName":"CJ",
        "lastName":"Mays",
        "equity":"0",
        "firstSeen":"8/19/2014 3:00:00",
        "lastSeen":"8/24/2014 3:00:00"
    },
    {
        "firstName":"Kyle",
        "lastName":"McAlpin",
        "equity":"-49",
        "firstSeen":"7/29/2012 3:00:00",
        "lastSeen":"7/29/2012 3:00:00"
    },
    {
        "firstName":"Scotty",
        "lastName":"McCrystal",
        "equity":"19",
        "firstSeen":"",
        "lastSeen":"11/6/2014 3:00:00"
    },
    {
        "firstName":"Mac",
        "lastName":"McDonald",
        "equity":"34",
        "firstSeen":"",
        "lastSeen":"8/2/2012 3:00:00"
    },
    {
        "firstName":"James",
        "lastName":"McFarland",
        "equity":"-84",
        "firstSeen":"3/10/2013 4:00:00",
        "lastSeen":"3/10/2013 4:00:00"
    },
    {
        "firstName":"Tom",
        "lastName":"McManus",
        "equity":"16",
        "firstSeen":"9/14/2014 3:00:00",
        "lastSeen":"9/21/2014 3:00:00"
    },
    {
        "firstName":"Dean",
        "lastName":"McMahan",
        "equity":"8",
        "firstSeen":"12/14/2014 3:00:00",
        "lastSeen":"12/14/2014 3:00:00"
    },
    {
        "firstName":"Katelyn",
        "lastName":"McNamara",
        "equity":"54",
        "firstSeen":"2/4/2015 3:00:00",
        "lastSeen":"2/25/2015 3:00:00"
    },
    {
        "firstName":"Walter",
        "lastName":"Meade",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Charlie",
        "lastName":"Mendoza",
        "equity":"96",
        "firstSeen":"6/18/2012 3:00:00",
        "lastSeen":"9/10/2013 3:00:00"
    },
    {
        "firstName":"Brandon",
        "lastName":"Mesecher",
        "equity":"4",
        "firstSeen":"7/9/2013 3:00:00",
        "lastSeen":"8/13/2013 3:00:00"
    },
    {
        "firstName":"Dugan",
        "lastName":"Meyer",
        "equity":"8",
        "firstSeen":"8/14/2014 3:00:00",
        "lastSeen":"8/14/2014 3:00:00"
    },
    {
        "firstName":"Clif",
        "lastName":"Meyers",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Ray",
        "lastName":"Merrill",
        "equity":"10",
        "firstSeen":"",
        "lastSeen":"12/7/2014 3:00:00"
    },
    {
        "firstName":"Luke",
        "lastName":"Miles",
        "equity":"20",
        "firstSeen":"1/5/2014 3:00:00",
        "lastSeen":"1/5/2014 3:00:00"
    },
    {
        "firstName":"Celeste",
        "lastName":"Miller",
        "equity":"16",
        "firstSeen":"9/11/2012 3:00:00",
        "lastSeen":"9/11/2012 3:00:00"
    },
    {
        "firstName":"Emily",
        "lastName":"Miller",
        "equity":"12",
        "firstSeen":"7/25/2013 3:00:00",
        "lastSeen":"7/25/2013 3:00:00"
    },
    {
        "firstName":"Linda",
        "lastName":"Miller",
        "equity":"-24",
        "firstSeen":"7/22/2012 3:00:00",
        "lastSeen":"7/22/2012 3:00:00"
    },
    {
        "firstName":"Luther",
        "lastName":"Miller",
        "equity":"44",
        "firstSeen":"9/3/2013 3:00:00",
        "lastSeen":"12/12/2013 3:00:00"
    },
    {
        "firstName":"Damian",
        "lastName":"Minarik",
        "equity":"60",
        "firstSeen":"9/3/2014 3:00:00",
        "lastSeen":"3/18/2015 3:00:00"
    },
    {
        "firstName":"Chad",
        "lastName":"Moore",
        "equity":"-75",
        "firstSeen":"8/27/2013 3:00:00",
        "lastSeen":"8/27/2013 3:00:00"
    },
    {
        "firstName":"James",
        "lastName":"Morrison",
        "equity":"-19",
        "firstSeen":"9/23/2012 3:00:00",
        "lastSeen":"9/23/2012 3:00:00"
    },
    {
        "firstName":"Stevie",
        "lastName":"Morrison",
        "equity":"35",
        "firstSeen":"9/31/12",
        "lastSeen":"5/12/2013 3:00:00"
    },
    {
        "firstName":"Art",
        "lastName":"Morten",
        "equity":"-6",
        "firstSeen":"7/7/2013 3:00:00",
        "lastSeen":"7/7/2013 3:00:00"
    },
    {
        "firstName":"Anthony",
        "lastName":"Morter",
        "equity":"-15",
        "firstSeen":"8/4/2013 3:00:00",
        "lastSeen":"4/24/2014 3:00:00"
    },
    {
        "firstName":"Lee",
        "lastName":"Moser",
        "equity":"38",
        "firstSeen":"9/18/2012 3:00:00",
        "lastSeen":"11/27/2012 3:00:00"
    },
    {
        "firstName":"Matthew",
        "lastName":"Moss",
        "equity":"-75",
        "firstSeen":"4/28/2013 3:00:00",
        "lastSeen":"4/28/2013 3:00:00"
    },
    {
        "firstName":"Ted",
        "lastName":"Mullinix",
        "equity":"4",
        "firstSeen":"7/16/2013 3:00:00",
        "lastSeen":"7/16/2013 3:00:00"
    },
    {
        "firstName":"Dan",
        "lastName":"Mulligan",
        "equity":"152",
        "firstSeen":"",
        "lastSeen":"3/22/2015 3:00:00"
    },
    {
        "firstName":"Fe",
        "lastName":"Myers",
        "equity":"12",
        "firstSeen":"2/11/2015 3:00:00",
        "lastSeen":"2/11/2015 3:00:00"
    },
    {
        "firstName":"Mark",
        "lastName":"Murray",
        "equity":"6",
        "firstSeen":"4/2/2013 3:00:00",
        "lastSeen":"7/28/2013 3:00:00"
    },
    {
        "firstName":"Erin",
        "lastName":"Napier",
        "equity":"36",
        "firstSeen":"2/4/2015",
        "lastSeen":"2/11/2015"
    },
    {
        "firstName":"Carnell",
        "lastName":"Nash",
        "equity":"2",
        "firstSeen":"9/8/2013",
        "lastSeen":"9/15/2013"
    },
    {
        "firstName":"David",
        "lastName":"Noble",
        "equity":"4",
        "firstSeen":"2/14/2013",
        "lastSeen":"4/7/2013"
    },
    {
        "firstName":"Zak",
        "lastName":"Norton",
        "equity":"41",
        "firstSeen":"",
        "lastSeen":"11/18/2014"
    },
    {
        "firstName":"Tom",
        "lastName":"OBrien",
        "equity":"69",
        "firstSeen":"",
        "lastSeen":"10/1/2014"
    },
    {
        "firstName":"Bronson",
        "lastName":"O' Quinn",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Charles",
        "lastName":"O'Bryan",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Tyler",
        "lastName":"Offerman",
        "equity":"44",
        "firstSeen":"2/4/2015",
        "lastSeen":"2/25/2015"
    },
    {
        "firstName":"Eric",
        "lastName":"Ogle",
        "equity":"0",
        "firstSeen":"8/22/2013",
        "lastSeen":"8/22/2013"
    },
    {
        "firstName":"Pieder",
        "lastName":"Otten",
        "equity":"30",
        "firstSeen":"1/8/2012",
        "lastSeen":"7/22/2014"
    },
    {
        "firstName":"Bethany",
        "lastName":"Overfield",
        "equity":"184",
        "firstSeen":"6/4/2012",
        "lastSeen":"8/23/2012"
    },
    {
        "firstName":"Antiono",
        "lastName":"Overstreet",
        "equity":"-4",
        "firstSeen":"6/11/2013",
        "lastSeen":"10/21/2014"
    },
    {
        "firstName":"Dave",
        "lastName":"Overton",
        "equity":"499",
        "firstSeen":"11/17/2011",
        "lastSeen":"5/20/2012"
    },
    {
        "firstName":"Evan",
        "lastName":"Palmer",
        "equity":"52",
        "firstSeen":"1/19/2012",
        "lastSeen":"8/26/2012"
    },
    {
        "firstName":"James",
        "lastName":"Parr",
        "equity":"100",
        "firstSeen":"2/22/2015",
        "lastSeen":"3/15/2015"
    },
    {
        "firstName":"Josh",
        "lastName":"Parrish",
        "equity":"88",
        "firstSeen":"11/1/2011",
        "lastSeen":"10/23/2012"
    },
    {
        "firstName":"undefined",
        "lastName":"Pasfique",
        "equity":"29",
        "firstSeen":"5/30/2013",
        "lastSeen":"7/3/2014"
    },
    {
        "firstName":"Asna",
        "lastName":"Patel",
        "equity":"104",
        "firstSeen":"11/20/2011",
        "lastSeen":"1/15/2012"
    },
    {
        "firstName":"Julia",
        "lastName":"Peckinpaugh",
        "equity":"46",
        "firstSeen":"11/1/2011",
        "lastSeen":"11/8/2011"
    },
    {
        "firstName":"undefined",
        "lastName":"Peanut",
        "equity":"0",
        "firstSeen":"10/8/2014",
        "lastSeen":"10/8/2014"
    },
    {
        "firstName":"Caleb",
        "lastName":"Pence",
        "equity":"32",
        "firstSeen":"6/30/2013",
        "lastSeen":"6/30/2013"
    },
    {
        "firstName":"Jessica",
        "lastName":"Pence",
        "equity":"16",
        "firstSeen":"6/30/2013",
        "lastSeen":"6/30/2013"
    },
    {
        "firstName":"Tim",
        "lastName":"Pennix",
        "equity":"64",
        "firstSeen":"11/20/2011",
        "lastSeen":"12/4/2011"
    },
    {
        "firstName":"Ronald",
        "lastName":"Penny",
        "equity":"24",
        "firstSeen":"6/9/2013",
        "lastSeen":"6/9/2013"
    },
    {
        "firstName":"Curtis",
        "lastName":"Perkins",
        "equity":"8",
        "firstSeen":"",
        "lastSeen":"3/18/2012"
    },
    {
        "firstName":"Mike",
        "lastName":"Peters",
        "equity":"5",
        "firstSeen":"11/10/2011",
        "lastSeen":"7/21/2013"
    },
    {
        "firstName":"Shawnquel",
        "lastName":"Phillips",
        "equity":"10",
        "firstSeen":"11/13/2014",
        "lastSeen":"12/21/2014"
    },
    {
        "firstName":"Shawahnia?",
        "lastName":"Phillis",
        "equity":"8",
        "firstSeen":"8/12/2014",
        "lastSeen":"8/12/2014"
    },
    {
        "firstName":"Paul",
        "lastName":"Phua",
        "equity":"40",
        "firstSeen":"12/18/2011",
        "lastSeen":"6/23/2013"
    },
    {
        "firstName":"Michael",
        "lastName":"Piepgrass",
        "equity":"16",
        "firstSeen":"2/4/2015",
        "lastSeen":"2/4/2015"
    },
    {
        "firstName":"James K",
        "lastName":"Pierson",
        "equity":"6",
        "firstSeen":"6/26/2012",
        "lastSeen":"4/16/2013"
    },
    {
        "firstName":"Kevin",
        "lastName":"Pinkston",
        "equity":"0",
        "firstSeen":"7/16/2012",
        "lastSeen":"7/24/2012"
    },
    {
        "firstName":"Robert",
        "lastName":"Pinson",
        "equity":"0",
        "firstSeen":"2/25/2015",
        "lastSeen":"2/26/2015"
    },
    {
        "firstName":"Dylan",
        "lastName":"Poindexter",
        "equity":"30",
        "firstSeen":"8/26/2012",
        "lastSeen":"8/26/2012"
    },
    {
        "firstName":"David",
        "lastName":"Priest",
        "equity":"52",
        "firstSeen":"11/20/2014",
        "lastSeen":"11/23/2014"
    },
    {
        "firstName":"David",
        "lastName":"Porch",
        "equity":"2",
        "firstSeen":"2/12/2013",
        "lastSeen":"2/17/2013"
    },
    {
        "firstName":"Gary",
        "lastName":"Pusey",
        "equity":"143",
        "firstSeen":"",
        "lastSeen":"?"
    },
    {
        "firstName":"Cy",
        "lastName":"Pugh",
        "equity":"12",
        "firstSeen":"5/12/2013",
        "lastSeen":"10/26/2013"
    },
    {
        "firstName":"James",
        "lastName":"Price",
        "equity":"2",
        "firstSeen":"",
        "lastSeen":"4/17/2014"
    },
    {
        "firstName":"Hua",
        "lastName":"Qing",
        "equity":"38",
        "firstSeen":"8/4/2013",
        "lastSeen":"10/6/2013"
    },
    {
        "firstName":"Kim",
        "lastName":"Ramirez",
        "equity":"4",
        "firstSeen":"",
        "lastSeen":"3/6/2012"
    },
    {
        "firstName":"Rebecca",
        "lastName":"Ramsey",
        "equity":"44",
        "firstSeen":"12/11/2012",
        "lastSeen":"8/13/2013"
    },
    {
        "firstName":"James",
        "lastName":"Ramsey",
        "equity":"124",
        "firstSeen":"12/11/2012",
        "lastSeen":"8/13/2013"
    },
    {
        "firstName":"charles",
        "lastName":"Ramsey",
        "equity":"24",
        "firstSeen":"10/3/2013",
        "lastSeen":"10/3/2013"
    },
    {
        "firstName":"Mike",
        "lastName":"Rayburn",
        "equity":"24",
        "firstSeen":"2/5/2012",
        "lastSeen":"3/1/2012"
    },
    {
        "firstName":"Edgar",
        "lastName":"Rayon",
        "equity":"0",
        "firstSeen":"6/23/2013",
        "lastSeen":"6/23/2013"
    },
    {
        "firstName":"Michael",
        "lastName":"Reichart",
        "equity":"0",
        "firstSeen":"12/3/2013",
        "lastSeen":"12/3/2013"
    },
    {
        "firstName":"Brandon",
        "lastName":"Render",
        "equity":"60",
        "firstSeen":"8/19/2014",
        "lastSeen":"9/7/2014"
    },
    {
        "firstName":"Charles",
        "lastName":"Richardson",
        "equity":"-10",
        "firstSeen":"8/24/2014",
        "lastSeen":"8/24/2014"
    },
    {
        "firstName":"Alex",
        "lastName":"Rickman",
        "equity":"5",
        "firstSeen":"9/10/2013",
        "lastSeen":"9/19/2013"
    },
    {
        "firstName":"Rich",
        "lastName":"Riffitt",
        "equity":"7",
        "firstSeen":"3/3/2013",
        "lastSeen":"11/10/2013"
    },
    {
        "firstName":"Jakel",
        "lastName":"Riley",
        "equity":"4",
        "firstSeen":"8/14/2014",
        "lastSeen":"9/15/2014"
    },
    {
        "firstName":"John Carl",
        "lastName":"Rinck",
        "equity":"0",
        "firstSeen":"6/6/2013",
        "lastSeen":"7/7/2013"
    },
    {
        "firstName":"Onias",
        "lastName":"Riggs",
        "equity":"66",
        "firstSeen":"",
        "lastSeen":"7/11/2013"
    },
    {
        "firstName":"Zethan",
        "lastName":"Riggs",
        "equity":"12",
        "firstSeen":"7/29/2012",
        "lastSeen":"8/24/2014"
    },
    {
        "firstName":"Jose",
        "lastName":"Rivera",
        "equity":"0",
        "firstSeen":"3/3/2013",
        "lastSeen":"3/3/2013"
    },
    {
        "firstName":"Mason",
        "lastName":"Roberts",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"T",
        "lastName":"Robertson",
        "equity":"-30",
        "firstSeen":"",
        "lastSeen":"8/19/2014"
    },
    {
        "firstName":"William",
        "lastName":"Robinson",
        "equity":"4",
        "firstSeen":"4/22/2014",
        "lastSeen":"9/21/2014"
    },
    {
        "firstName":"Robert",
        "lastName":"Robinson",
        "equity":"-16",
        "firstSeen":"",
        "lastSeen":"10/2/2014"
    },
    {
        "firstName":"Peter",
        "lastName":"Rock",
        "equity":"76",
        "firstSeen":"",
        "lastSeen":"1/31/2012"
    },
    {
        "firstName":"Steve",
        "lastName":"Roggenkamp",
        "equity":"40",
        "firstSeen":"3/14/2013",
        "lastSeen":"9/17/2014"
    },
    {
        "firstName":"Brent",
        "lastName":"Ross",
        "equity":"76",
        "firstSeen":"",
        "lastSeen":"3/18/2015"
    },
    {
        "firstName":"Cecilia",
        "lastName":"Ross",
        "equity":"90",
        "firstSeen":"",
        "lastSeen":"3/18/2015"
    },
    {
        "firstName":"Shaneem",
        "lastName":"Rush",
        "equity":"-18",
        "firstSeen":"4/10/2014",
        "lastSeen":"4/10/2014"
    },
    {
        "firstName":"Ethan",
        "lastName":"Rutledge",
        "equity":"32",
        "firstSeen":"",
        "lastSeen":""
    },
    {
        "firstName":"Marc",
        "lastName":"Rowley",
        "equity":"84",
        "firstSeen":"10/3/2013",
        "lastSeen":"11/21/2013"
    },
    {
        "firstName":"Rachel",
        "lastName":"Rowley",
        "equity":"84",
        "firstSeen":"10/3/2013",
        "lastSeen":"11/21/2013"
    },
    {
        "firstName":"Jennifer",
        "lastName":"Ryall",
        "equity":"48",
        "firstSeen":"2/2/2012",
        "lastSeen":"5/13/2012"
    },
    {
        "firstName":"Thad",
        "lastName":"Salmon",
        "equity":"28",
        "firstSeen":"7/10/2012",
        "lastSeen":"7/12/2012"
    },
    {
        "firstName":"Capone",
        "lastName":"Sanders",
        "equity":"-30",
        "firstSeen":"9/7/2014",
        "lastSeen":"10/8/2014"
    },
    {
        "firstName":"Pedro",
        "lastName":"Santiago",
        "equity":"0",
        "firstSeen":"9/21/2011",
        "lastSeen":"10/20/2013"
    },
    {
        "firstName":"Jonathan",
        "lastName":"Schaeffer",
        "equity":"174",
        "firstSeen":"10/9/2011",
        "lastSeen":"2/26/2012"
    },
    {
        "firstName":"Jack",
        "lastName":"Schmidt",
        "equity":"24",
        "firstSeen":"2/3/2013",
        "lastSeen":"2/10/2013"
    },
    {
        "firstName":"Chris",
        "lastName":"Scherrer",
        "equity":"12",
        "firstSeen":"9/12/2013",
        "lastSeen":"2/2/2014"
    },
    {
        "firstName":"Cherokee",
        "lastName":"Schill",
        "equity":"130",
        "firstSeen":"12/3/2013",
        "lastSeen":"9/26/2014"
    },
    {
        "firstName":"Nathan",
        "lastName":"Schill",
        "equity":"24",
        "firstSeen":"9/28/2014",
        "lastSeen":"9/28/2014"
    },
    {
        "firstName":"William",
        "lastName":"Scott",
        "equity":"8",
        "firstSeen":"7/15/2014",
        "lastSeen":"7/15/2014"
    },
    {
        "firstName":"Michael",
        "lastName":"Seagraves",
        "equity":"5",
        "firstSeen":"7/29/2012",
        "lastSeen":"7/25/2013"
    },
    {
        "firstName":"Martin",
        "lastName":"Shearer",
        "equity":"70",
        "firstSeen":"3/21/2013",
        "lastSeen":"5/7/2013"
    },
    {
        "firstName":"Jill",
        "lastName":"Shearer",
        "equity":"10",
        "firstSeen":"5/7/2013",
        "lastSeen":"5/7/2013"
    },
    {
        "firstName":"Jeff",
        "lastName":"Sheffield",
        "equity":"12",
        "firstSeen":"11/27/2012",
        "lastSeen":"11/27/2012"
    },
    {
        "firstName":"Grant",
        "lastName":"Shemonic",
        "equity":"16",
        "firstSeen":"",
        "lastSeen":"3/13/2012"
    },
    {
        "firstName":"Andy",
        "lastName":"Shooner",
        "equity":"250",
        "firstSeen":"",
        "lastSeen":"3/1/2015"
    },
    {
        "firstName":"Timothy",
        "lastName":"Shepherd",
        "equity":"16",
        "firstSeen":"8/19/2014",
        "lastSeen":"8/19/2014"
    },
    {
        "firstName":"Josh",
        "lastName":"Shuler",
        "equity":"16",
        "firstSeen":"10/19/2014",
        "lastSeen":"10/19/2014"
    },
    {
        "firstName":"Bruce",
        "lastName":"Sims",
        "equity":"0",
        "firstSeen":"8/11/2013",
        "lastSeen":"9/15/2013"
    },
    {
        "firstName":"Jacob",
        "lastName":"Slaughter",
        "equity":"224",
        "firstSeen":"11/20/2011",
        "lastSeen":"5/13/2014"
    },
    {
        "firstName":"Jeffrey",
        "lastName":"Slaughter",
        "equity":"0",
        "firstSeen":"3/12/2013",
        "lastSeen":"3/12/2013"
    },
    {
        "firstName":"Doug",
        "lastName":"Slaymaker",
        "equity":"183",
        "firstSeen":"9/11/2011",
        "lastSeen":"11/13/2011"
    },
    {
        "firstName":"Devan",
        "lastName":"Smith",
        "equity":"0",
        "firstSeen":"",
        "lastSeen":"7/29/2012"
    },
    {
        "firstName":"Jason",
        "lastName":"Smith",
        "equity":"-8",
        "firstSeen":"5/14/2013",
        "lastSeen":"9/7/2014"
    },
    {
        "firstName":"John",
        "lastName":"Smith",
        "equity":"13",
        "firstSeen":"6/30/2013",
        "lastSeen":"7/7/2013"
    },
    {
        "firstName":"Paul",
        "lastName":"Smith",
        "equity":"0",
        "firstSeen":"5/18/2014",
        "lastSeen":"5/18/2014"
    },
    {
        "firstName":"Lisa",
        "lastName":"Smith",
        "equity":"0",
        "firstSeen":"5/18/2014",
        "lastSeen":"5/18/2014"
    },
    {
        "firstName":"Sarah",
        "lastName":"Smith",
        "equity":"8",
        "firstSeen":"",
        "lastSeen":"3/11/2012"
    },
    {
        "firstName":"Tito",
        "lastName":"Smith",
        "equity":"20",
        "firstSeen":"6/25/2013",
        "lastSeen":"6/25/2013"
    },
    {
        "firstName":"Victor",
        "lastName":"Smith",
        "equity":"250",
        "firstSeen":"6/28/2011",
        "lastSeen":"1/22/2013"
    },
    {
        "firstName":"Andrew",
        "lastName":"Sowell",
        "equity":"16",
        "firstSeen":"1/13/2013",
        "lastSeen":"1/13/2013"
    },
    {
        "firstName":"Rick",
        "lastName":"Staggs",
        "equity":"110",
        "firstSeen":"3/10/2013",
        "lastSeen":"8/12/2014"
    },
    {
        "firstName":"Mike",
        "lastName":"Sternberger",
        "equity":"250",
        "firstSeen":"",
        "lastSeen":"8/8/2013"
    },
    {
        "firstName":"Andre",
        "lastName":"Stover",
        "equity":"123",
        "firstSeen":"8/26/2012",
        "lastSeen":"5/9/2013"
    },
    {
        "firstName":"Sophie",
        "lastName":"Strosberg",
        "equity":"56",
        "firstSeen":"",
        "lastSeen":"8/12/2012"
    },
    {
        "firstName":"Erin",
        "lastName":"Stricland",
        "equity":"12",
        "firstSeen":"9/30/2014",
        "lastSeen":"10/9/2014"
    },
    {
        "firstName":"David",
        "lastName":"Summers",
        "equity":"32",
        "firstSeen":"",
        "lastSeen":"8/24/2014"
    },
    {
        "firstName":"Jessica",
        "lastName":"Sutherland",
        "equity":"56",
        "firstSeen":"",
        "lastSeen":"2/25/2015"
    },
    {
        "firstName":"Terio",
        "lastName":"Sutherland",
        "equity":"56",
        "firstSeen":"",
        "lastSeen":"2/25/2015"
    },
    {
        "firstName":"David",
        "lastName":"Takahashi",
        "equity":"60",
        "firstSeen":"9/9/2014",
        "lastSeen":"12/2/2014"
    },
    {
        "firstName":"Bob",
        "lastName":"Tarne",
        "equity":"20",
        "firstSeen":"2/25/2015",
        "lastSeen":"2/25/2015"
    },
    {
        "firstName":"James",
        "lastName":"Thomas",
        "equity":"32",
        "firstSeen":"7/2/2013",
        "lastSeen":"7/2/2013"
    },
    {
        "firstName":"Martha",
        "lastName":"Tillson",
        "equity":"44",
        "firstSeen":"6/13/2013",
        "lastSeen":"6/25/2013"
    },
    {
        "firstName":"Harlan",
        "lastName":"Tipton",
        "equity":"48",
        "firstSeen":"10/29/2013",
        "lastSeen":"11/5/2013"
    },
    {
        "firstName":"George",
        "lastName":"Tolliver",
        "equity":"10",
        "firstSeen":"8/3/2014",
        "lastSeen":"8/3/2014"
    },
    {
        "firstName":"Ann",
        "lastName":"Towzen",
        "equity":"50",
        "firstSeen":"2/4/2015",
        "lastSeen":"3/11/2015"
    },
    {
        "firstName":"Jeff",
        "lastName":"Towzen",
        "equity":"16",
        "firstSeen":"2/4/2015",
        "lastSeen":"2/4/2015"
    },
    {
        "firstName":"Joey",
        "lastName":"Tucci",
        "equity":"18",
        "firstSeen":"6/7/2012",
        "lastSeen":"6/7/2012"
    },
    {
        "firstName":"Casey",
        "lastName":"Turner",
        "equity":"38",
        "firstSeen":"10/28/2014",
        "lastSeen":"11/6/2014"
    },
    {
        "firstName":"John",
        "lastName":"Tyler",
        "equity":"16",
        "firstSeen":"",
        "lastSeen":"8/31/2014"
    },
    {
        "firstName":"William",
        "lastName":"Varney",
        "equity":"92",
        "firstSeen":"10/1/2013",
        "lastSeen":"2/4/2014"
    },
    {
        "firstName":"Alex",
        "lastName":"Vickers",
        "equity":"44",
        "firstSeen":"12/3/2013",
        "lastSeen":"12/10/2013"
    },
    {
        "firstName":"Joseph",
        "lastName":"Vieira",
        "equity":"80",
        "firstSeen":"5/20/2012",
        "lastSeen":"5/22/2012"
    },
    {
        "firstName":"Carl",
        "lastName":"Vogel",
        "equity":"250",
        "firstSeen":"11/17/2011",
        "lastSeen":"12/10/2013"
    },
    {
        "firstName":"Mike",
        "lastName":"Waltman",
        "equity":"256",
        "firstSeen":"",
        "lastSeen":"8/16/2012"
    },
    {
        "firstName":"Samantha",
        "lastName":"Wang",
        "equity":"24",
        "firstSeen":"11/27/2011",
        "lastSeen":"11/27/2011"
    },
    {
        "firstName":"John",
        "lastName":"Walker",
        "equity":"282",
        "firstSeen":"1/5/2014",
        "lastSeen":"11/17/2013"
    },
    {
        "firstName":"John",
        "lastName":"Wahl",
        "equity":"100",
        "firstSeen":"4/13/2014",
        "lastSeen":"7/27/2014"
    },
    {
        "firstName":"Josh",
        "lastName":"Watkins",
        "equity":"-8",
        "firstSeen":"7/24/2014",
        "lastSeen":"7/24/2014"
    },
    {
        "firstName":"Thomas",
        "lastName":"Watt.",
        "equity":"36",
        "firstSeen":"",
        "lastSeen":"7/16/2013"
    },
    {
        "firstName":"John",
        "lastName":"Weaver",
        "equity":"165",
        "firstSeen":"1/15/2012",
        "lastSeen":"3/15/2015"
    },
    {
        "firstName":"Jerry",
        "lastName":"Weisenfluh",
        "equity":"32",
        "firstSeen":"11/20/2011",
        "lastSeen":"11/20/2011"
    },
    {
        "firstName":"Michael",
        "lastName":"Weitlaur",
        "equity":"6",
        "firstSeen":"7/1/2014",
        "lastSeen":"7/1/2014"
    },
    {
        "firstName":"Brent",
        "lastName":"Werner",
        "equity":"8",
        "firstSeen":"7/13/2014",
        "lastSeen":"9/25/2014"
    },
    {
        "firstName":"Andy",
        "lastName":"Werner",
        "equity":"48",
        "firstSeen":"1/19/2012",
        "lastSeen":"2/16/2012"
    },
    {
        "firstName":"Lane",
        "lastName":"Whitehorn",
        "equity":"71",
        "firstSeen":"10/26/2013",
        "lastSeen":"11/7/2013"
    },
    {
        "firstName":"Frank",
        "lastName":"Whiting",
        "equity":"26",
        "firstSeen":"5/12/2013",
        "lastSeen":"5/14/2013"
    },
    {
        "firstName":"Bob",
        "lastName":"Williams",
        "equity":"-8",
        "firstSeen":"2/4/2015",
        "lastSeen":"2/4/2015"
    },
    {
        "firstName":"Josh",
        "lastName":"Williams",
        "equity":"40",
        "firstSeen":"4/2/2013",
        "lastSeen":"4/9/2013"
    },
    {
        "firstName":"Jaelin",
        "lastName":"Williams",
        "equity":"28",
        "firstSeen":"8/31/2014",
        "lastSeen":"9/9/2014"
    },
    {
        "firstName":"Kiya",
        "lastName":"Williams",
        "equity":"36",
        "firstSeen":"8/31/2014",
        "lastSeen":"9/7/2014"
    },
    {
        "firstName":"Bettie",
        "lastName":"Wilson",
        "equity":"48",
        "firstSeen":"10/8/2013",
        "lastSeen":"10/13/2013"
    },
    {
        "firstName":"Byron",
        "lastName":"Wilson",
        "equity":"4",
        "firstSeen":"9/2/2014",
        "lastSeen":"9/2/2014"
    },
    {
        "firstName":"Hallie",
        "lastName":"Wilson",
        "equity":"0",
        "firstSeen":"10/8/2013",
        "lastSeen":"10/10/2013"
    },
    {
        "firstName":"Ree",
        "lastName":"Wilson",
        "equity":"8",
        "firstSeen":"7/17/2012",
        "lastSeen":"7/17/2012"
    },
    {
        "firstName":"Jacob",
        "lastName":"Winters",
        "equity":"24",
        "firstSeen":"2/16/2012",
        "lastSeen":"2/16/2012"
    },
    {
        "firstName":"Zach",
        "lastName":"Winterman",
        "equity":"-50",
        "firstSeen":"4/28/2013",
        "lastSeen":"4/28/2013"
    },
    {
        "firstName":"Elli",
        "lastName":"Wright",
        "equity":"16",
        "firstSeen":"6/30/2013",
        "lastSeen":"6/30/2013"
    },
    {
        "firstName":"CC",
        "lastName":"Wright",
        "equity":"139",
        "firstSeen":"7/1/2014",
        "lastSeen":"7/8/2014"
    },
    {
        "firstName":"Nathan",
        "lastName":"Wright",
        "equity":"216",
        "firstSeen":"1/13/2015",
        "lastSeen":"3/18/2015"
    },
    {
        "firstName":"Mike",
        "lastName":"Wurth",
        "equity":"44",
        "firstSeen":"4/10/2014",
        "lastSeen":"1/29/2015"
    },
    {
        "firstName":"Joe",
        "lastName":"Yan",
        "equity":"240",
        "firstSeen":"",
        "lastSeen":"6/23/2013"
    },
    {
        "firstName":"Ben",
        "lastName":"Yates",
        "equity":"29",
        "firstSeen":"12/29/2011",
        "lastSeen":"9/2/2012"
    },
    {
        "firstName":"Dustin",
        "lastName":"Young",
        "equity":"15",
        "firstSeen":"3/21/2013",
        "lastSeen":"3/21/2013"
    },
    {
        "firstName":"Jeremy",
        "lastName":"Young",
        "equity":"1",
        "firstSeen":"2/2/2014",
        "lastSeen":"3/18/2015"
    },
    {
        "firstName":"Nathan",
        "lastName":"Zamarron",
        "equity":"-3",
        "firstSeen":"6/19/2012",
        "lastSeen":"6/19/2012"
    },
    {
        "firstName":"Matt",
        "lastName":"Zoot",
        "equity":"-12",
        "firstSeen":"8/12/2014",
        "lastSeen":"8/12/2014"
    }
]
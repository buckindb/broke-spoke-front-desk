function filteredPublish(collection, handle){
    Meteor.publish(handle, function(filter, cursor, sort, pageLength){
        _.each(filter, function(val, key, list){
            if (val.$regex){
                filter[key] = {$regex: new RegExp(val.$regex, 'i')}
            }
        });
        
        Counts.publish(this, handle+'Count', collection.find(filter), {noReady:true});
        return collection.find(
            filter, 
            {
                limit:pageLength, 
                skip: cursor, 
                sort: sort, 
                reactive:true
            }
            );

    })
}

// This is ending up in the blaze template data property...
var collections = [
        {handle: 'peopleList', object:People},
        {handle: 'peopleRecord', object:People},
        {handle: 'timelogsList', object:Timelogs},
        {handle: 'timelogRecord', object:Timelogs},
        {handle: 'transactionsList', object:Transactions},
        {handle: 'transactionRecord', object:Transactions},
        //{handle:'bikesList', object:Bikes},
        //{handle:'bikesRecord', object:Bikes}
    ]

_.each(collections,function(collection, i, list){
        filteredPublish(collection.object, collection.handle);
});


//People AUTOCOMPLETE
Meteor.publish('autocomplete-people', function(selector, options, collName) {
  collection = People;

  if (options.limit) {
    options.limit = Math.min(50, Math.abs(options.limit));
  }
  Autocomplete.publishCursor(collection.find(selector, options), this);
  return this.ready();
});


Meteor.publish('allUsers', function() {
    return Meteor.users.find();
});

//// publish posts
Meteor.publish('currentlySignedIn', function(limit) {
    return Timelogs.find(
        {endTime:{$exists:false}}
    )
});

Meteor.publish('recentlySignedIn', function(limit) {
    var d = new Date();
    d.setHours(0,0,0,0);
    return Timelogs.find(
        {endTime:{$gte:d}}
    )
});

Meteor.publish('recentlyCreatedPeople', function() {
    var d = new Date();
    d.setHours(0,0,0,0);
    return People.find(
        {createdOn:{$gte:d}}
    )
});

Meteor.publish('signInPerson', function(personId){
    return People.find(personId);
})

Meteor.publish('singleTimelog', function(timelogId){
    return Timelogs.find(timelogId);
})


Meteor.publish('pendingTransactions', function(limit) {
    return Transactions.find(
        {paymentStatus:"pending", paymentType:'cash'}
    )
});


//Reporting

//Monthly Volunteers

Meteor.publish('monthlyVisitors', function(reportYear, reportMonth){
        var pipeline = [
            {   $project:{
                    year: {$year:"$startTime"},
                    month: {$month:"$startTime"}
            }},
            {
                $match: {
                    year: reportYear,
                    month: reportMonth
                }
            },
            {
                $group: {_id: personId}
            }
        ];
    })


//Monthly Visitors
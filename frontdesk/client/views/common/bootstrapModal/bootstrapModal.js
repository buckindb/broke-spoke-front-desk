Template.bootstrapModal.helpers({

    //modal: function(){ return {
    //    show: function(){console.log('show modal!', this)}
    //}},


});

Template.bootstrapModal.created = function(){
}

Template.bootstrapModal.rendered = function(){
    BootstrapModal.setModalView(this);
}

//Modal singleton
BootstrapModal = (function(){
    var instance;

    function init() {
        var view,
        contentTemplate;

        return {
            setModalView: function(viewInstance){
                this.view = viewInstance;
            },

            show: function(templateName, templateData){
                //TODO: implement Blaze.remove on previous child view
                //crude clearing in dom
               // $('#bootstrapModal .modal.content').empty();
                this.view = this.view || {};
                this.view.templateName;
                this.view.data = templateData || this.view.data || {};

                //render the template

               this.view.$('.modal-content').empty();

              //  if (templateData){
                    Blaze.renderWithData(Template[templateName], this.view.data, this.view.$('.modal-content')[0]);
                //} else {
                //    Blaze.render(Template[templateName], this.view.$('.modal-content')[0]);
                //}


                //show the modal
                $(this.view.firstNode).modal(
                    {
                        backdrop:'static'
                });

                $(this.view.firstNode).modal('show');
            },
            hide: function(){
                $(this.view.firstNode).modal('hide');
               // Blaze.remove(Template[this.view.templateName]);
            },
        }

    }

    return function(){
                if (!instance){
                    instance = init();
                }
                return instance;
    }();



})();
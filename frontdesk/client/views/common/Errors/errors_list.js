Template.meteorErrors.helpers({
  errors: function() {
    return Errors.collection.find();
  }
});

Template.meteorError.rendered = function() {
  var error = this.data;
    var alertDom = this.$('.alert');
    if (this.data.timeout){
        setTimeout(
            function(){
                alertDom.alert('close');
            },
        this.data.timeout);

    }
  Meteor.defer(function() {
    Errors.collection.update(error._id, {$set: {seen: true}});
  });
};

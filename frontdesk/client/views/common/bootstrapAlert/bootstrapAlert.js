Template.bootstrapAlerts.helpers({
    alerts: function() {
        return BootstrapAlert.collection.find();
    }
});

Template.bootstrapAlert.rendered = function() {
    var alert = this.data;
    var alertDom = this.$('.alert');
    if (this.data.timeout){
        setTimeout(
            function(){
                alertDom.alert('close');
            },
            this.data.timeout
        );
    }
    Meteor.defer(function() {
        BootstrapAlert.collection.update(alert._id, {$set: {seen: true}});
    });
};


BootstrapAlert = {
    // Local (client-only) collection
    collection: new Meteor.Collection(null),

    throw: function(message, timeout, type) {
        var type = type || 'danger';
        BootstrapAlert.collection.insert({message: message, type: type, timeout:timeout, seen:false}, function(err){console.log('cb', err)})
    },
    clearSeen: function() {
        BootstrapAlert.collection.remove({seen: true});
    }
};


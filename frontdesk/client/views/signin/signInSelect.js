Template.signInSelect.helpers({
    //now: function () {
    //    return moment().format("YYYY-MM-DDTHH:mm");
    //},

    //Name autocomplete
    settings: function(){return {
        position: 'bottom',
        limit: 10,
        rules: [
            {
                // token: '',
                collection: 'People',
                subscription: 'autocomplete-people',
                field: 'lastName',
                matchAll: true,
                template: Template.timelogsPersonAutocomplete,
            }
        ],

    }},
    people: function() {
        return People.find();
    }
});


Template.signInSelect.created = function(){

};


Template.signInSelect.events({

    'click .register' : function(event){
        BootstrapModal.show('registerModal');
    },

    'autocompleteselect input': function(event, template, doc){
        $('#personAutocomplete').val('');
        Session.set('signInId', doc._id);
        if (doc.waiverAcceptDate){ //todo: add an annual or other limit for waiver review
            BootstrapModal.show('activitySelectModal');
        } else {
            BootstrapModal.show('waiverModal');
        }

    }
})
Template.paymentOptions.created = function () {
    // 1. Initialization

    var instance = this;

    // initialize the reactive variables
    instance.loaded = new ReactiveVar(0);
    instance.ready = new ReactiveVar(false);

    // 2. Autorun

    // will re-run when the "limit" reactive variables changes
    instance.autorun(function () {

        // subscribe to the posts publication
        var subscription = Meteor.subscribe('singleTimelog', Session.get('signOutTimelogId'));


        // if subscription is ready, set limit to newLimit
        if (subscription.ready()) {

            instance.ready.set(true);
        } else {
            instance.ready.set(false);
        }

    });

    // 3. Cursor
    instance.timelog = function() {
        return Timelogs.findOne(Session.get('signOutTimelogId'));
    };

    instance.person = function(){
        return People.findOne(Session.get('signInId'));
    }

}

Template.paymentOptions.helpers({
    // the posts cursor
    timelog: function () {
        return Template.instance().timelog();
    },

   person: function(){
       return Template.instance().person();
   },

    // the subscription handle
    isReady: function () {
        return Template.instance().ready.get();
    },

    //Todo: these should be handled by allowing the timelog to be ended and a pending transaction to be created.
    endTime: function(){
        return new Date();
    },

    amount: function(){
        var timelog = Timelogs.findOne(Session.get('signOutTimelogId'));
        var rate = appSettings.activityTypes.get(timelog.activityType).rate;
        return Math.abs(timelog.totalTime * rate);
    },

    goodCredit: function(){
        if (People.findOne(Template.instance().timelog().personId).sweatEquity < 0){
            return false;
        }
        return true;
    }
})

Template.paymentOptions.events({

    "click .payment": function(event, template){
        Timelogs.update( Session.get('signOutTimelogId'),{$set:{paymentType: $(event.currentTarget).data('payment')}}, function(err){
            if (err){
                BootstrapModal.hide();
                BootstrapAlert.throw(err.message, 0, 'danger');
            } else {
                BootstrapModal.hide();
                //BootstrapAlert.throw("Success", 500, 'success');
                Session.set('signOutTimelogId', null);
                Session.set('signInId', null);
            }
        })

    },


})
Template.signOutList.created = function () {
    // 1. Initialization

    var instance = this;

    // initialize the reactive variables
    instance.loaded = new ReactiveVar(0);
    instance.ready = new ReactiveVar(false);

    // 2. Autorun

    // will re-run when the "limit" reactive variables changes
    instance.autorun(function () {

        // subscribe to the posts publication
        var subscription = Meteor.subscribe('currentlySignedIn');

        // if subscription is ready, set limit to newLimit
        if (subscription.ready()) {

            instance.ready.set(true);
        } else {
            instance.ready.set(false);
        }

    });

    // 3. Cursor
    instance.timelogs = function() {
        return Timelogs.find(
            {endTime:{$exists:false}}
        )
    };
}


Template.signOutList.helpers({
    // the posts cursor
    timelogs: function () {
        return Template.instance().timelogs();
    },

    // the subscription handle
    isReady: function () {
        return Template.instance().ready.get();
    },
})

Template.signOutList.events({
    'click .signout': function(event){
        var timelog = Timelogs.findOne($(event.currentTarget).parents('tr').data("id"));
        Session.set('signOutTimelogId', timelog._id);
        Session.set('signInId', timelog.personId);

        var now = new Date();
        Timelogs.update($(event.currentTarget).parents('tr').data("id"), {$set:{endTime:now}}, {}, function(err){
            if (err){
                Errors.throw(err.message, 4000);
            } else {
                if (timelog.activityType == 'stand-time'){
                    BootstrapModal.show('paymentOptions');
                } else {
                    //todo: this should be able to just pass the timelog object, but I think it would be a clone form before the edit.
                    BootstrapModal.show('signOutSummary');
                    setTimeout(function () {
                        BootstrapModal.hide();
                    }, 5000)
                }
            }
        });
    }
})
appSettings.defaultVars.setDefaults(['peopleCursor', 'peopleFilter', 'peopleSort', 'peoplePageLength']);

Meteor.autorun(function(){
    Meteor.subscribe(
        'peopleList',
        Session.get('peopleFilter'),
        Session.get('peopleCursor'),
        Session.get('peopleSort'),
        Session.get('peoplePageLength')
    );
});

Template.peopleList.helpers({
    people: function () {
        return People.find(
            {}, {
                sort: Session.get('peopleSort'),
                limit: Session.get('peoplePageLength') }
        );
    },

    pages: function(){
        return {
            total: function(){
                return Math.ceil(Session.get('peopleListCount') / Session.get('peoplePageLength'));
            },
            current: function(){
                return  (Session.get('peopleCursor') /  Session.get('peoplePageLength') + 1);
            },
            show: function() {
                if (Math.ceil(Session.get('peopleListCount') / Session.get('peoplePageLength')) > 1){
                    return true
                }
                return false;
            }
        }
    },

});

Template.peopleList.rendered = function(){
    appSettings.defaultVars.setDefaults(['peopleCursor', 'peopleFilter', 'peopleSort', 'peoplePageLength']);


    this.autorun(function(){

        Session.set('peopleListCount', Counts.get('peopleListCount'));
        if (Session.get('peopleCursor') == 0){
            $('.previous').addClass('disabled').removeClass('enabled');
        } else {
            $('.previous').removeClass('disabled').addClass('enabled');
        }

        if (Session.get('peopleCursor') >= (Session.get('peopleListCount') - Session.get('peoplePageLength'))) {
            $('.next').addClass('disabled').removeClass('enabled');
        } else {
            $('.next').removeClass('disabled').addClass('enabled');
        }

    })
};

Template.peopleList.events({

    'click .table tbody tr': function(event){
        Router.go('peopleAdminEdit', {personId:$(event.target).parents('tr').data("id") });
    },

    'click .table tbody tr .delete': function(event, temp) {
        var doc = People.findOne( { _id: $(event.target).parents('tr').data("id") } );
        BootstrapModal.show('peopleConfirmDelete', {person:doc});
        event.stopPropagation();
    },

    'click .new': function(event, template){
        Router.go('peopleAdminNew');
    },

    'click .enabled.previous': function(event, template){
        if (Session.get('peopleCursor') >= 1){
            Session.set('peopleCursor', Session.get('peopleCursor') - Session.get('peoplePageLength'));
        }
    },

    'click .enabled.next': function(event, template){
        Session.set('peopleCursor', Session.get('peopleCursor') + Session.get('peoplePageLength'));
    },

    'keyup input#lastNameFilter': function(event, template){

        var filters = Session.get('peopleFilter'),
            value = $('#lastNameFilter').val(),
            re = value;

        filters.lastName = {$regex: re};
        Session.set('peopleCursor', 0);
        Session.set('peopleFilter', filters);
    }

})
//TODO: implement this for all uses of autocomplete

Template.personAutocomplete.helpers({

    settings: function(){return {
        position: 'bottom',
        limit: 10,
        rules: [
            {
                collection: 'People',
                subscription: 'autocomplete-people',
                field: 'lastName',
                matchAll: true,
                template: Template.personAutocompleteItem
            }
        ],

    }},
    people: function() {
        return People.find();
    }
})

Template.personAutocomplete.events({
    'autocompleteselect input': function(event, template, doc){
        $('input[name=personId]').val(doc._id);
        $('.-autocomplete-container').hide();
        $(event.currentTarget).hide().val('');
        $('.selectedPerson .name').text(doc.firstName + " " + doc.lastName);
        $('.selectedPerson').show();
    },
    'click .clear': function(event){
        $('input[name=personId]').val('');
        $('.selectedPerson').hide();
        $('.selectedPerson .name').text('');
        $('#personAutocomplete').show();
        event.stopPropagation();
    },
})


Template.personAutocompleteItem.helpers({

    isMember: function(){

        if (new Date() < this.membershipExpiration){
            return true;
        }
        return false;
    }
})
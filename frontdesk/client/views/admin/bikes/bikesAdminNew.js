Template.bikesAdminNew.helpers({
    now:function(){
        return moment().format("YYYY-MM-DD");
    },

    bikes: function(){
        return Bikes.find();
    },

    settings: function(){return {
        position: 'bottom',
        limit: 10,
        rules: [
            {
                // token: '',
                collection: 'People',
                subscription: 'autocomplete-people',
                field: 'lastName',
                matchAll: true,
                template: Template.timelogsPersonAutocomplete
            }
        ],

    }},
    people: function() {
        return People.find();
    }
})


Template.bikesAdminNew.events({

    'click .clear': function(){
        $('input[name=owner]').val('');
        $('.selectedPerson').hide();
        $('.selectedPerson .name').text('');
        $('#personAutocomplete').show();
    },

    'autocompleteselect input': function(event, template, doc){
        $('input[name=ownerId]').val(doc._id);
        $('.-autocomplete-container').hide();
        $(event.currentTarget).hide().val('');
        $('.selectedPerson .name').text(doc.firstName + " " + doc.lastName);
        $('.selectedPerson').show();
    }

})


Template.timelogsAdminNew.rendered = function(){

}
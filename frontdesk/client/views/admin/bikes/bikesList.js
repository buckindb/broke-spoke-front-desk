appSettings.defaultVars.setDefaults(['bikesCursor', 'bikesFilter', 'bikesSort', 'bikesPageLength']);

Meteor.autorun(function(){

    Meteor.subscribe(
        'bikesList',
        Session.get('bikesFilter'),
        Session.get('bikesCursor'),
        Session.get('bikesSort'),
        Session.get('bikesPageLength')
    );
});


Template.bikesList.helpers({
    bikes: function () {
        return Bikes.find(
            {}, {
                sort: Session.get('bikesSort'),
                limit: Session.get('bikesPageLength') }
        );
    },

    pages: function(){
        return {
            total: function(){
                return Math.ceil(Session.get('bikesListCount') / Session.get('bikesPageLength'));
            },
            current: function(){
                return  (Session.get('bikesCursor') /  Session.get('bikesPageLength') + 1);
            },
            show: function() {
                if (Math.ceil(Session.get('bikesListCount') / Session.get('bikesPageLength')) > 1){
                    return true
                }
                return false;
            }
        }
    },

});


Template.bikesList.rendered = function(){
    appSettings.defaultVars.setDefaults(['bikesCursor', 'bikesFilter', 'bikesSort', 'bikesPageLength']);

    this.autorun(function(){

        Session.set('bikesListCount', Counts.get('bikesListCount'));
        if (Session.get('bikesCursor') == 0){
            $('.previous').addClass('disabled').removeClass('enabled');
        } else {
            $('.previous').removeClass('disabled').addClass('enabled');
        }

        if (Session.get('bikesCursor') >= (Session.get('bikesListCount') - Session.get('bikesPageLength'))) {
            $('.next').addClass('disabled').removeClass('enabled');
        } else {
            $('.next').removeClass('disabled').addClass('enabled');
        }

    })
};

Template.bikesList.events({

    'click .table tbody tr': function(event){
        Router.go('bikesAdminEdit', {personId:$(event.target).parents('tr').data("id") });
    },

    'click .table tbody tr .delete': function(event, temp) {
        var doc = Bikes.findOne( { _id: $(event.target).parents('tr').data("id") } );
        BootstrapModal.show('bikesConfirmDelete', {person:doc});
        event.stopPropagation();
    },

    'click .new': function(event, template){
        Router.go('bikesAdminNew');
    },

    'click .enabled.previous': function(event, template){
        if (Session.get('bikesCursor') >= 1){
            Session.set('bikesCursor', Session.get('bikesCursor') - Session.get('bikesPageLength'));
        }
    },

    'click .enabled.next': function(event, template){
        Session.set('bikesCursor', Session.get('bikesCursor') + Session.get('bikesPageLength'));
    },

    'keyup input#tagIDFilter': function(event, template){

        var filters = Session.get('bikesFilter'),
            value = $('#tagIDFilter').val(),
            re = value;

        filters.tagID = {$regex: re};
        Session.set('bikesCursor', 0);
        Session.set('bikesFilter', filters);
    }

})
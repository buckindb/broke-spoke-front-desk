Template.quickTransactionModal.helpers({

})

Template.quickTransactionModal.helpers({
    now: function () {
        return moment().format("YYYY-MM-DDTHH:mm");
    },

    transactionOptions:[
        {label:"Equity Bike Purchase", value:'bike-purchase'},
        {label:"Equity Parts Purchase", value:'parts-purchase'}
    ],

    later: function(){
        return moment().add(1, 'hours').format("YYYY-MM-DDT21:00");
    },

    //Name autocomplete
    settings: function(){return {
        position: 'bottom',
        limit: 10,
        rules: [
            {
                collection: 'People',
                subscription: 'autocomplete-people',
                field: 'lastName',
                matchAll: true,
                template: Template.quickSignInPersonAutocomplete
            }
        ],

    }},
    people: function() {
        return People.find();
    }

});
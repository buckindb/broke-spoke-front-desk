Template.recentSignIn.created = function () {
    // 1. Initialization

    var instance = this;

    // initialize the reactive variables
    instance.loaded = new ReactiveVar(0);
    instance.ready = new ReactiveVar(false);

    // 2. Autorun

    // will re-run when the "limit" reactive variables changes
    instance.autorun(function () {

        // subscribe to the posts publication
        var subscription = Meteor.subscribe('recentlySignedIn');

        // if subscription is ready, set limit to newLimit
        if (subscription.ready()) {
            instance.ready.set(true);
        } else {
            instance.ready.set(false);
        }

        //UI
        $('[data-toggle="tooltip"]').tooltip();
    });

    // 3. Cursor

    instance.timelogs = function() {
        var d = new Date();
        d.setHours(0,0,0,0);

        return Timelogs.find(
            {endTime:{$gte:d}},
            {sort:{endTime:-1}}
        )
    };

}

Template.recentSignIn.helpers({
    // the posts cursor
    timelogs: function () {
        return Template.instance().timelogs();
    },


    // the subscription handle
    isReady: function () {
        return Template.instance().ready.get();
    },
})

Template.recentSignIn.events({
    'click .table tbody tr': function(event){
        Router.go('timelogsAdminEditTimelog', {timelogId:$(event.target).parents('tr').data("id") });
    },
});

Template.pendingTransactions.created = function () {
    // 1. Initialization

    var instance = this;

    // initialize the reactive variables
    instance.loaded = new ReactiveVar(0);
    instance.ready = new ReactiveVar(false);

    // 2. Autorun

    // will re-run when the "limit" reactive variables changes
    instance.autorun(function () {

        // subscribe to the posts publication
        var subscription = Meteor.subscribe('pendingTransactions');

        // if subscription is ready, set limit to newLimit
        if (subscription.ready()) {

            instance.ready.set(true);
        } else {
            instance.ready.set(false);
        }

        ////UI
        //$('[data-toggle="tooltip"]').tooltip();
    });

    // 3. Cursor

    instance.transactions = function() {
            return Transactions.find(
                {paymentStatus:"pending", paymentType:"cash"}
            )

    };


}

Template.pendingTransactions.helpers({
    // the posts cursor
    transactions: function () {
        return Template.instance().transactions();
    },


    // the subscription handle
    isReady: function () {
        return Template.instance().ready.get();
    },
})

Template.pendingTransactions.events({
    'click .complete': function(event){
        Transactions.update($(event.currentTarget).parents('tr').data("id"), {$set:{paymentStatus:"complete"}})
    }
})
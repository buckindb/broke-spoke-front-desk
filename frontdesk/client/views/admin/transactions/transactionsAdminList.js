


//
//
//Template.transactionsAdminList.helpers({
//
//  transactions: function () {
//    var transactions = Transactions.find(
//        {},
//        {
//        sort: Session.get('transactionsSort'),
//        limit: Session.get('transactionsPageLength') }
//    );
//    return transactions;
//  },
//  pages: function(){
//      return {
//        total: function(){
//          return Math.ceil(Session.get('transactionsCount') / Session.get('transactionsPageLength'));
//        },
//        current: function(){
//         return  (Session.get('transactionsCursor') /  Session.get('transactionsPageLength') + 1);
//        }
//      }
//  }
//});
//
//
//Template.transactionsAdminList.rendered = function(){
//  appSettings.defaultVars.setDefaults(['transactionsCursor', 'transactionsFilter', 'transactionsSort', 'transactionsPageLength']);
//
//  this.autorun(function(){
//      Session.set('transactionsListCount', Counts.get('transactionsListCount'));
//      if (Session.get('transactionsCursor') == 0){
//         $('.previous').addClass('disabled').removeClass('enabled');
//      } else {
//         $('.previous').removeClass('disabled').addClass('enabled');
//      }
//
//  if (Session.get('transactionsCursor') >= (Session.get('transactionsListCount') - Session.get('transactionsPageLength'))) {
//        $('.next').addClass('disabled').removeClass('enabled');
//      } else {
//        $('.next').removeClass('disabled').addClass('enabled');
//      }
//
//  })
//};
//
//Template.transactionsAdminList.events({
//    'click .table tbody tr': function(event){
//        Router.go('transactionsAdminEdit', {transactionId:$(event.target).parents('tr').data("id") });
//    },
//
//  'click .delete': function(event, temp) {
//      console.log('click');
//    var Transaction = Transactions.findOne( { _id: $(event.target).parents('tr').data("id") } );
//
//    Session.set('adminModalContent',
//       {_id: Transaction._id,
//       title: "Delete this transaction entry?",
//        message: "This should be replaced by a message about deleting the related timelog."
//      }
//    );
//    $('#adminModal').modal('show');
//  },
//
//    'click .new': function(event, template){
//        Router.go('transactionsAdminNew');
//  },
//
//  'click #adminModal .cancel': function(event, template){
//      $('#adminModal').attr('data-recordid', "");
//      $('#adminModal').modal('hide');
//  },
//
//  'click #adminModal .confirm': function(event, template){
//        Transactions.remove($('#adminModal').attr('data-recordid'), function(err){console.log("deleted", err)});
//      //close the modal
//      $('#adminModal').modal('hide');
//  },
//
//  'click .enabled.previous': function(event, template){
//      if (Session.get('transactionsCursor') >= 1){
//          Session.set('transactionsCursor', Session.get('transactionsCursor') - Session.get('transactionsPageLength'));
//      }
//  },
//
//  'click .enabled.next': function(event, template){
//       Session.set('transactionsCursor', Session.get('transactionsCursor') + Session.get('transactionsPageLength'));
//  },
//})



Template.transactionsAdminEdit.helpers({
  //AUTOCOMPLETE STUFF
    settings: function(){return {
        position: 'bottom',
        limit: 10,
        rules: [
          {
            // token: '',
            collection: 'People',
            subscription: 'autocomplete-people',
            field: 'lastName',
            matchAll: true,
            template: Template.timelogsPersonAutocomplete,
          }
        ],

  }},
  
    people: function() {
        return People.find();
    }
    
});

Template.transactionsAdminEdit.events({
  
    'click .clear': function(){
      $('input[name=personId]').val('');
      $('.selectedPerson').hide();
      $('.selectedPerson .name').text('');
      $('#personAutocomplete').show();
    },

    //'click .cancel': function(){
    //    window.history.back();
    //},

    'change #transactionType': function(){
        if ($("#transactionType").val() === "volunteer-credit"){
            $("#paymentType").parents('.form-group').hide();
            $('#paymentType').val('equity');
            $("#paymentStatus").parents('.form-group').hide();
            $('#paymentStatus').val('complete');
        } else {
            $("#paymentType").parents('.form-group').show();
        }
    },

    'change #paymentType': function(){
        if ($("#paymentType").val() === "equity"){
            $("#paymentStatus").parents('.form-group').hide();
            $('#paymentStatus').val('complete');
        } else {
            $("#paymentStatus").parents('.form-group').show();
        }
    },



    'autocompleteselect input': function(event, template, doc){
        $('input[name=personId]').val(doc._id);
        $('.-autocomplete-container').hide();
        $(event.currentTarget).hide().val('');
        $('.selectedPerson .name').text(doc.firstName + " " + doc.lastName);
        $('.selectedPerson').show();
    }


});

Template.transactionsAdminEdit.rendered = function(){
    //If the route is person-specific and loaded the person record,
    //load the name as the value and don't allow it to change.
    if (this.data.person){
        var doc = this.data.person;
        $('input[name=personId]').val(doc._id);
        $('#personAutocomplete').hide().val('');
        $('.selectedPerson .name').text(doc.firstName + " " + doc.lastName);
        $('.selectedPerson .delete').hide();
        $('.selectedPerson').show();
    };

    if ($("#transactionType").val() === "volunteer-credit"){
        $("#paymentType").parents('.form-group').hide();
    }

    if ($("#paymentType").val() === "equity"){
        $("#paymentStatus").parents('.form-group').val("complete").hide();
    }
};
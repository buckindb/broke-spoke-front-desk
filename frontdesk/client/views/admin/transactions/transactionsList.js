appSettings.defaultVars.setDefaults(['transactionsCursor', 'transactionsFilter', 'transactionsSort', 'transactionsPageLength']);

Meteor.autorun(function(){
    Meteor.subscribe(
        'transactionsList',
        Session.get('transactionsFilter'),
        Session.get('transactionsCursor'),
        Session.get('transactionsSort'),
        Session.get('transactionsPageLength')
    );

});

Template.transactionsList.helpers({

    transactions: function () {
        var transactions = Transactions.find(
            {},
            {
                sort: Session.get('transactionsSort'),
                limit: Session.get('transactionsPageLength') }
        );
        return transactions;
    },
    pages: function(){
        return {
            total: function(){
                return Math.ceil(Session.get('transactionsListCount') / Session.get('transactionsPageLength'));
            },
            current: function(){
                return  (Session.get('transactionsCursor') /  Session.get('transactionsPageLength') + 1);
            },

            show: function() {
                if (Math.ceil(Session.get('transactionsListCount') / Session.get('transactionsPageLength')) > 1){
                    return true
                }
                return false;
            }
        }
    }
});


Template.transactionsList.rendered = function(){
    appSettings.defaultVars.setDefaults(['transactionsCursor', 'transactionsFilter', 'transactionsSort', 'transactionsPageLength']);
    this.autorun(function(){
        Session.set('transactionsListCount', Counts.get('transactionsListCount'));
        if (Session.get('transactionsCursor') === 0){
            $('.previous').addClass('disabled').removeClass('enabled');
        } else {
            $('.previous').removeClass('disabled').addClass('enabled');
        }

        if (Session.get('transactionsCursor') >= (Session.get('transactionsListCount') - Session.get('transactionsPageLength'))) {
            $('.next').addClass('disabled').removeClass('enabled');
        } else {
            $('.next').removeClass('disabled').addClass('enabled');
        }

    })
};

Template.transactionsList.events({
    'click .table tbody tr': function(event){
        var params = {};
        var transaction = Transactions.findOne($(event.target).parents('tr').data("id"));
        if (Router.current().params.personId){
            params.personId = Router.current().params.personId;
        }

        if (transaction.timelogId){
            params.timelogId = transaction.timelogId;
            if (Template.currentData().routeRoot === "transactionsAdmin"){
                Router.go('timelogsAdminEditTimelog', params);
            } else {
                Router.go(Template.currentData().routeRoot+'EditTimelog', params);
            }

        } else {
            params.transactionId = transaction._id;
            Router.go(Template.currentData().routeRoot+'EditTransaction', params);
        }
    },

    'click .delete': function(event, temp) {;
        var doc = Transactions.findOne( { _id: $(event.target).parents('tr').data("id") } );
        BootstrapModal.show('transactionConfirmDelete', {transaction:doc});
        event.stopPropagation();
    },

    'click .new': function(event, template){
        params={};
        if (Router.current().params.personId){
            params.personId = Router.current().params.personId;
        }
        Router.go(Template.currentData().routeRoot + 'NewTransaction', params);
        event.preventDefault();
    },


    'click .enabled.previous': function(event, template){
        if (Session.get('transactionsCursor') >= 1){
            Session.set('transactionsCursor', Session.get('transactionsCursor') - Session.get('transactionsPageLength'));
        }
    },

    'click .enabled.next': function(event, template){
        Session.set('transactionsCursor', Session.get('transactionsCursor') + Session.get('transactionsPageLength'));
    },
})
Template.timelogsConfirmDelete.events({

    'click .delete': function(){
        Timelogs.remove(this.timelog._id);
        BootstrapModal.hide();
    },

})
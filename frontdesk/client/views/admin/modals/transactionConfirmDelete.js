Template.transactionConfirmDelete.events({

    'click .delete': function(){
        Transactions.remove(this.transaction._id);
        BootstrapModal.hide();
    },

})
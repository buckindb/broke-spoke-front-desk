Template.peopleConfirmDelete.events({

    'click .delete': function(){
        People.remove(this.person._id);
        BootstrapModal.hide();
    },

})
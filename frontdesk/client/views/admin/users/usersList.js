Meteor.autorun(function(){
  Meteor.subscribe('allUsers'); 
});

Template.usersList.helpers({
   
   users:function(){ return Meteor.users.find()} 
    
});



Template.usersList.events({
    'click .delete': function(event, temp) {
    var User = Meteor.users.findOne( { _id: $(event.target).parents('tr').data("id") } );

    Session.set('adminModalClass', 'userDelete')

    Session.set('adminModalContent', 
       {_id: User._id,
       title: "Delete this user?",
        message: "This user will be removed from the system."
      }
    );
    $('#adminModal').modal('show');
  },
  
  'click #adminModal.userDelete .cancel': function(event, template){
      $('#adminModal').attr('data-recordid', "");
      $('#adminModal').modal('hide');
  },
  
  'click #adminModal.userDelete .confirm': function(event, template){
      Meteor.users.remove($('#adminModal').attr('data-recordid'), function(err){console.log('delete', err)});
      $('#adminModal').modal('hide');
  },

  'click .new': function(event, template){
    Session.set('adminModalClass', 'userNew')
    Session.set('adminModalTemplate', 'newUserModal')
    Session.set('adminModalContent', 
       {
       title: "Create new system user?",
       message: "Put the user form here..."
      }
    );
    $('#adminModal').modal('show');
  }
})
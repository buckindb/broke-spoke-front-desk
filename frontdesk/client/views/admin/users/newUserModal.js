Template.newUserModal.events({
    'click .cancel': function(event, template){
      $('#adminModal').attr('data-recordid', "");
      $('#adminModal').modal('hide');
  },
  
  'submit form': function(event, template){
      event.preventDefault();
      var email = template.$("input[name=email]").val(),
      username = template.$("input[name=username]").val(),
      password = template.$("input[name=password]").val(),
      passwordConfirm = template.$("input[name=passwordConfirm]").val(),
      role = template.$("input[name=role]").val();
      
      //validation
      if (password != passwordConfirm){
          //populate validation error to match UI
          return false;
      }
      
      
      //TODO: add error handler & error notice to modal
      id = Accounts.createUser({
          username:username,
          email: email,
          password:password
      });

      Roles.addUsersToRoles(id, role, Roles.GLOBAL_GROUP);
      $("#adminModal").hide();
  }
    
});

Template.newUserModal.helpers({
 //data context is provided by adminModal
});
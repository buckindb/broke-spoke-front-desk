  appSettings.defaultVars.setDefaults(
  ['timelogsCursor', 
  'timelogsFilter', 
  'timelogsSort', 
  'timelogsPageLength']);

Meteor.autorun(function(){
   Meteor.subscribe(
    'timelogsList', 
    Session.get('timelogsFilter'), 
    Session.get('timelogsCursor'), 
    Session.get('timelogsSort'),
    Session.get('timelogsPageLength')
  ); 
});

Template.timelogsList.helpers({
  
  timelogs: function () {
    return Timelogs.find({}, {
      sort:Session.get('timelogsSort')
    });
  },
   pages: function(){
      return {
        total: function(){
          return Math.ceil(Session.get('timelogsCount') / Session.get('timelogsPageLength'));
        },
        current: function(){
         return  (Session.get('timelogsCursor') /  Session.get('timelogsPageLength') + 1);
        },

          show: function() {
              if (Math.ceil(Session.get('timelogsCount') / Session.get('timelogsPageLength')) > 1){
                  return true
              }
              return false;
          }
      }
  }

});

Template.timelogsList.rendered = function(){
  
  appSettings.defaultVars.setDefaults(
  ['timelogsCursor', 
  'timelogsFilter', 
  'timelogsSort', 
  'timelogsPageLength']);


  this.autorun(function(){

      Session.set('timelogsCount', Counts.get('timelogsListCount'));
      if (Session.get('timelogsCursor') === 0){
        $('.previous').addClass('disabled').removeClass('enabled');
      } else {
         $('.previous').removeClass('disabled').addClass('enabled');
      }

      if (Session.get('timelogsCursor') >= (Session.get('timelogsCount') - Session.get('timelogsPageLength'))) {
        $('.next').addClass('disabled').removeClass('enabled');
      } else {
        $('.next').removeClass('disabled').addClass('enabled');
      }
      
  })
};


Template.timelogsList.events({
  
    'click .table tbody tr': function(event){

        var params = {timelogId:$(event.target).parents('tr').data("id")};

        if (Router.current().params.personId){
            params.personId = Router.current().params.personId;
        }

        Router.go(Template.currentData().routeRoot+'EditTimelog', params);
    },
  
    'click .delete': function(event, temp) {
    var doc = Timelogs.findOne( { _id: $(event.target).parents('tr').data("id") } );
    BootstrapModal.show('timelogsConfirmDelete', {timelog:doc});
    event.stopPropagation();
  },
  
  'click .new': function(event, template){
      params={};
      if (Router.current().params.personId){
          params.personId = Router.current().params.personId;
      }
      Router.go(Template.currentData().routeRoot + 'NewTimelog', params);
      event.preventDefault();
  },

  'click .enabled.previous': function(event, template){
      if (Session.get('timelogsCursor') >= 1){
        Session.set('timelogsCursor', Session.get('timelogsCursor') - Session.get('timelogsPageLength'));
      }
  },
  
  'click .enabled.next': function(event, template){
    Session.set('timelogsCursor', Session.get('timelogsCursor') + Session.get('timelogsPageLength'));
  },
})

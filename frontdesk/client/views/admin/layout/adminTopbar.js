Template.adminTopbar.events({
    'click #admin-logout': function(){
        Meteor.logout();
        Router.render('login');
    }
  
});

Template.adminTopbar.helpers({
    username: function(){
        return Meteor.user().username;
    }
});
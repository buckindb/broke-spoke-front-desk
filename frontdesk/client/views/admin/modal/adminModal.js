Template.adminModal.helpers({
    class:  function(){return Session.get('adminModalClass')},
    content: function(){return Session.get('adminModalContent')},
    template: function(){return Session.get('adminModalTemplate')},
})


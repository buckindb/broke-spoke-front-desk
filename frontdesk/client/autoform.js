AutoForm.hooks({
  insertPeopleForm: {
    onSuccess: function(operation, result, template) {
        window.history.back();
    }, 
  },
    updatePeopleForm: {
    onSuccess: function(operation, result, template) {
        window.history.back();
    }, 
  },
  insertTransactionsForm: {
    onSuccess: function(operation, result, template) {
        window.history.back();
    }, 
    formToDoc: function(doc, ss, formId) {
        if (!doc.personId){
          $('.form-group.person').addClass('has-error');
          $('.form-group.person .help-block').text('Person is required');
        } else {
          $('.form-group.person').removeClass('has-error');
          $('.form-group.person .help-block').text('');
        }
        return doc;
    },

      before:{
          insert:  function(doc){
              if (appSettings.transactionTypes.get(doc.transactionType) && appSettings.transactionTypes.get(doc.transactionType).direction == "debit"){
                  doc.amount = -(doc.amount);
              }
              return doc;

          }
      }
  },

    insertTransactionQuick: {
        before: {
            insert:  function(doc){
                if (appSettings.transactionTypes.get(doc.transactionType) && appSettings.transactionTypes.get(doc.transactionType).direction == "debit"){
                    doc.amount = -(doc.amount);
                }
                return doc;

            }
        },

        onSuccess: function(operation, result, template) {
            BootstrapModal.hide();
        },
        formToDoc: function(doc, ss, formId) {
            if (!doc.personId){
                $('.form-group.person').addClass('has-error');
                $('.form-group.person .help-block').text('Person is required');
            } else {
                $('.form-group.person').removeClass('has-error');
                $('.form-group.person .help-block').text('');
            }
            return doc;
        }
    },

  updateTransactionsForm: {
      onSuccess: function(operation, result, template) {
          window.history.back();
      },

      docToForm: function(doc, ss, formId){
        if (appSettings.transactionTypes.get(doc.transactionType).direction == "debit"){
            doc.amount = -(doc.amount);
        }
          return doc;
      },
      formToDoc: function(doc, ss, formId) {
        if (!doc.personId){
          $('.form-group.person').addClass('has-error');
          $('.form-group.person .help-block').text('Person is required');
        } else {
          $('.form-group.person').removeClass('has-error');
          $('.form-group.person .help-block').text('');
        }
        return doc;
    },

      before:{
          update:  function(doc){
              if (appSettings.transactionTypes.get(doc.$set.transactionType) && appSettings.transactionTypes.get(doc.$set.transactionType).direction == "debit"){
                  doc.$set.amount = -(doc.$set.amount);
              }

              return doc;
          }
      }

  },
    insertTimelogsForm: {

    before: {
        insert: function(doc, template){
            return doc;
        }
    },

    onSuccess: function(operation, result, template) {
        window.history.back();
    },   
    formToDoc: function(doc, ss, formId) {
        if (!doc.personId){
          $('.form-group.person').addClass('has-error');
          $('.form-group.person .help-block').text('Person is required');
        } else {
          $('.form-group.person').removeClass('has-error');
          $('.form-group.person .help-block').text('');
        }
        return doc;
    }
  },
    insertTimelogsQuick: {

        before: {
            insert: function(doc, template){
                return doc;
            }
        },

        onSuccess: function(operation, result, template) {
            BootstrapModal.hide();
        },
        formToDoc: function(doc, ss, formId) {
            if (!doc.personId){
                $('.form-group.person').addClass('has-error');
                $('.form-group.person .help-block').text('Person is required');
            } else {
                $('.form-group.person').removeClass('has-error');
                $('.form-group.person .help-block').text('');
            }
            return doc;
        }
    },
  updateTimelogsForm: {
      onSuccess: function(operation, result, template) {
          window.history.back();
      }, 
      onError: function(operation, error, template) {
        console.error(error);
      },    
      formToDoc: function(doc, ss, formId) {
        if (!doc.personId){
          $('.form-group.person').addClass('has-error');
          $('.form-group.person .help-block').text('Person is required');
        } else {
          $('.form-group.person').removeClass('has-error');
          $('.form-group.person .help-block').text('');
        }
        return doc;
    }
  },

    insertBikeForm: {
        onSuccess: function(operation, result, template) {
            window.history.back();
        },
        onError: function(operation, error, template) {
            console.error(error);
        },
    },
  
  userAddForm: {
      onSuccess: function(operation, result, template) {
      $("#adminModal").hide();
      }, 
  },

    createAccountModalForm: {
        onSuccess: function(operation, result, template) {
            //Proceed to the waiver acceptance Modal
            Session.set('signInId', result);
            BootstrapModal.show('waiverModal')
        },

        onError: function(formType, error){
            if (error.invalidKeys || error.invalidKeys.length > 0){
                //validation error
            } else {
                //some other write error
                BootstrapModal.hide();
                BootstrapAlert.throw(error.message, 0, 'danger')
            }

        }

        //TODO: Really need to add duplicate name checking, and have a UI error here (and in admin)
    },

});


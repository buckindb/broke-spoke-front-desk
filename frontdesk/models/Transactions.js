Transactions = new Meteor.Collection("transactions");


Schemas = (typeof Schemas === 'undefined') ? {} : Schemas;

Schemas.Transactions = new SimpleSchema({
    personId: {
        type: String,
        label: "Person",
        autoform: {
            options: function () {
               return People.find().map(function (p) {
                  return {label: p.lastName + ", " +p.firstName , value: p._id};
                });
      }
    }
    } ,
    person: {
      type:Object,
      blackbox:true,
      optional:true
    },
    transactionType: {
        type:String,
        label: "Transaction Type",
         autoform: {
            options: appSettings.transactionTypes.items
        }
    },
    amount: {
        type: Number,
        label: "Amount",
        autoValue: function(){
            //reduce the amount to fit within equity limit
            if (this.field('transactionType').value =="volunteer-credit"){
                var person = People.findOne(this.field('personId').value);
                if (person.sweatEquity + this.value > 250){
                    return 250 - person.sweatEquity;
                }

            }
        },
        custom: function(){
            if (this.field('transactionType').value != "imported"){
                //TODO change this test to debit vs credit, to allow for future additional credit types
                if (this.field('amount').value < 0 && this.field('transactionType').value =="volunteer-credit"){
                    return 'positiveAmounts';
                } else if (this.field('amount').value > 0 && this.field('transactionType').value != "volunteer-credit"){
                    return 'positiveAmounts';
                }
            }
        }
    },

    timelogId: {
      type:String,
      optional:true
    },
    paymentType: {
      type:String,
      label: "Payment Type",
      autoform: {
          options:[
              { label:"Cash/Credit", value:"cash"},
              { label:"Sweat Equity", value:"equity"}
          ]
      },
        custom: function(){
            if (this.field('transactionType').value == "volunteer-credit" && this.value == "cash"){
                return 'volunteerCash';
            }
        }
    },
    paymentStatus: {
        type:String,
        label: "Payment Status",
        autoform: {
            options:[
                { label:"Complete", value:"complete"},
                { label:"Pending", value:"pending"}
            ]
        },
        custom: function(){
            if (this.field('paymentType').value == "equity" && this.value == "pending"){
                return 'volunteerPending';
            }
        },
        defaultValue:'complete'
    },
    date: {
        type: Date,
        label: "Date"
    },
    
    //Metadata
    createdOn: {
        type:Date,
        label: "Date created",
        autoValue: function() {
        if (this.isInsert) {
          return new Date;
        } else if (this.isUpsert) {
          return {$setOnInsert: new Date};
        } else {
          this.unset();
        }
      }
    },
   
   
});

Schemas.Transactions.messages({
    'volunteerCash': "'Volunteer Credit' transactions must have a payment type of 'Sweat Equity'.",
    'volunteerPending': "Only cash transactions may have a 'pending' status.",
    'positiveAmounts':"Transaction amounts must be positive (charges are applied based on 'Transaction Type').",
})

//permissions (simple check for admin)
Transactions.allow({
    insert: function(userId, transaction){
        return userId;
    },
    update: function(userId, transaction){
        return userId;
    },
    remove: function(userId, transaction){
        return userId;
    }
});

 
//Add hooks for reference syncing
Transactions.before.insert(function (userId, doc) {
    doc.person = People.findOne(doc.personId);
});

Transactions.after.insert(function (userId, doc, fieldNames, modifier, options) {
    Meteor.call('transactionsSumEquity', doc.personId);
});



//match the person doc to the personId
Transactions.before.update(function (userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.person = People.findOne(modifier.$set.personId);
    modifier.$set.transactionType = modifier.$set.transactionType || doc.transactionType;
    modifier.$set.amount = modifier.$set.amount || doc.amount;

    //if the payment type is being changed to cash, interpret that as a pending transaction unless it's specified as completed.
    if (doc.paymentType != "cash" && modifier.$set.paymentType == "cash" && modifier.$set.paymentStatus != "complete"){
        modifier.$set.paymentStatus = "pending"
    }
});

Transactions.after.update(function (userId, doc, fieldNames, modifier, options) {
    if (this.previous.personId != doc.personId){
        Meteor.call('transactionsSumEquity', this.previous.personId);
    }
    Meteor.call('transactionsSumEquity', doc.personId);
});


Transactions.after.remove(function (userId, doc) {
    if(Meteor.isServer){
        Timelogs.remove({transactionId: doc._id});
        Meteor.call('transactionsSumEquity', doc.personId);
    }
});



Transactions.attachSchema(Schemas.Transactions);

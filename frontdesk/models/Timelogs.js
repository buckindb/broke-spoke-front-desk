Timelogs = new Meteor.Collection("timelogs");

Schemas = (typeof Schemas === 'undefined') ? {} : Schemas;

Schemas.Timelogs = new SimpleSchema({
    personId: {
        type: String,
        label: "Person",
        autoform: {
            options: function () {
               return People.find().map(function (p) {
                  return {label: p.lastName + ", " +p.firstName , value: p._id};
            });
      }
    }
    } ,
    person: {
      type:Object,
      blackbox:true,
      optional:true
    },
    transactionId: {
        type:String,
        label: "Transaction Id",
        optional:true
    },
    activityType: {
        type: String,
        label: "Activity",
        autoform: {
            options:appSettings.activityTypes.items
        }
    },
    paymentType: {
      // model hack to get around the need to take payment
      // preference at the same time a saving a timelog
        type: String,
        label: "Payment type",
        optional: true,
        autoform: {
            options:[
                {label:"Sweat Equity", value:'equity'},
                {label:"Cash/Credit Card", value:'cash'}]
        },
        //todo: this would be better to have a 'pending' or null option
        custom: function () {
            if (this.field('activityType').value === 'stand-time' && this.field('endTime').isSet && !this.isSet && (!this.operator || (this.value === null || this.value === ""))) {
                return "required";
            }
        }
    },
    startTime: {
        type: Date,
        label: "Start Time"
    },
    endTime: {
        type: Date,
        label: "End Time",
        optional: true,
        autoValue: function(){
            if (this.value && this.field('startTime').value - this.value == 900000){
                return this.field('startTime').value;
                //
            }
        },
        custom: function(){
            if (this.value && this.field('startTime').value > this.value){
                return 'endTime';
            }
        }
    },
    totalTime: {
        type: Number,
        optional: true,
        label: "Total Time",
        decimal: true
    },
    
    //Metadata
    createdOn: {
        type:Date,
        label: "Date created",
        autoValue: function() {
        if (this.isInsert) {
          return new Date;
        } else if (this.isUpsert) {
          return {$setOnInsert: new Date};
        } else {
          this.unset();
        }
      }
    }
});

Schemas.Timelogs.messages({
    'endTime': "The end time cannot be earlier than the start time."
})

//permissions (simple check for admin)
Timelogs.allow({
    insert: function(userId, transaction){
        return userId;
    },
    update: function(userId, transaction){
        return userId;
    },
    remove: function(userId, transaction){
        return userId;
    }
});


Timelogs.before.insert(function (userId, doc) {



    //embed person doc
    doc.person = People.findOne(doc.personId);
    //round times
    //NOTE: Time rounding logic is repeated below in update hook
    var ROUNDING = 15 * 60 * 1000; /*ms*/
    start = moment(doc.startTime);
    end = moment(doc.endTime);
    
    if (doc.activityType == "volunteering"){
        start = moment(Math.floor((+doc.startTime) / ROUNDING) * ROUNDING).toDate();
        end = moment(Math.ceil((+doc.endTime) / ROUNDING) * ROUNDING).toDate();
    } else {
        start = moment(Math.ceil((+doc.startTime) / ROUNDING) * ROUNDING).toDate();
        end = moment(Math.floor((+doc.endTime) / ROUNDING) * ROUNDING).toDate();
    }
    
    doc.startTime = start;

    if (doc.endTime){
        //set totalTime
        doc.endTime = end;
        var totalHours = Math.ceil(Math.abs(end - start)/36e5 * 100)/100;
        doc.totalTime = totalHours;
    }

});

Timelogs.after.insert(function(userId, doc){
 //this was firing twice, I think on client + server
 if(Meteor.isServer){
     var activityType = appSettings.activityTypes.get(doc.activityType);
     var insertData = {
         personId: doc.personId,
         timelogId: doc._id,
         date: doc.endTime,
         amount: doc.totalTime * activityType.rate,

     };
     var transactionType = appSettings.transactionTypes.get(activityType.transactionType);

     if (transactionType){
         insertData.transactionType = transactionType.value;

         //TODO: implement broader pending status.
         //Not implemented: automate pending status if payment type isn't defined.
         //Currently won't work due to validation requiring payment type for transactions.
         // Pending status was originally only meant for cash transactions, but could be
         //useful for timelog-created transactions that haven't been assigned a payment type.
         //E.g., a manager canceling or zeroing a transaction for someone who has no way to pay.

         //if (transactionType.paymentType === "both"){
         //    if (!doc.paymentType){
         //        insertData.paymentType = "pending";
         //        insertData.paymentStatus = "pending";
         //    } else {
         //        insertData.paymentType =  doc.paymentType;
         //    }
         //} else {
         //    insertData.paymentType = transactionType.paymentType;
         //}

         insertData.paymentType = (transactionType.paymentType === "both") ? doc.paymentType : transactionType.paymentType;
     }

      if(doc.totalTime && transactionType ){
          Transactions.insert(insertData, function(err, transactionId){
              if (err){
                  console.log("Error inserting transaction:", err);
              } else {
                  Timelogs.direct.update({_id:doc._id}, {$set:{transactionId: transactionId}});
              }

           });

          //Update the person's latest sign-out
          var person = People.findOne(doc.personId);
          if (doc.endTime > (person.lastSignout || 0)){
              People.direct.update(doc.personId, {$set:{lastSignout: doc.endTime}}, function(err, i){console.log(err, i)});
          }
      }


 }
});

// //match the person doc to the personId
Timelogs.before.update(function (userId, doc, fieldNames, modifier, options) {

        modifier.$set = modifier.$set || {};
        modifier.$set.personId = modifier.$set.personId || doc.personId;
        modifier.$set.person = People.findOne(modifier.$set.personId);
        modifier.$set.startTime = modifier.$set.startTime || doc.startTime;
        //modifier.$set.startTime = modifier.$set.startTime || doc.startTime;
        modifier.$set.activityType = modifier.$set.activityType || doc.activityType;

        //round times
        var ROUNDING = 15 * 60 * 1000; /*ms*/
        start = moment(doc.startTime);
        end = moment(doc.endTime);
        if (modifier.$set.activityType == "volunteering"){
            start = moment(Math.floor((+modifier.$set.startTime) / ROUNDING) * ROUNDING).toDate();
            end = moment(Math.ceil((+modifier.$set.endTime) / ROUNDING) * ROUNDING).toDate();
        } else {
            start = moment(Math.ceil((+modifier.$set.startTime) / ROUNDING) * ROUNDING).toDate();
            end = moment(Math.floor((+modifier.$set.endTime) / ROUNDING) * ROUNDING).toDate();
        }

        modifier.$set.startTime = start;
        //set totalTime
        if (modifier.$set.endTime){
          modifier.$set.endTime = end;
          var totalHours = Math.ceil(Math.abs(modifier.$set.endTime - modifier.$set.startTime)/36e5 * 100)/100;
            modifier.$set.totalTime = totalHours;

        } else if (modifier.$unset && modifier.$unset.endTime===''){
            modifier.$unset.totalTime = "";
            if(doc.transactionId){
                if(Meteor.isServer) {
                    Transactions.direct.remove({_id: doc.transactionId});
                    modifier.$unset.transactionId = "";
                }
            }
        }
});

Timelogs.after.update(function (userId, doc) {
    //this was firing twice, I think on client + server
     if(Meteor.isServer){
          var activityType = appSettings.activityTypes.get(doc.activityType);
          var transactionType = appSettings.transactionTypes.get(activityType.transactionType);
         if (transactionType){
            var paymentType = (transactionType.paymentType === "both") ? doc.paymentType : transactionType.paymentType;
         }
          //If the timelog is complete, create or update a transaction
          if(doc.totalTime && transactionType){
              if (doc.transactionId){

                 Transactions.update({_id:doc.transactionId}, {$set:{
                   personId: doc.personId,
                   date: doc.endTime,
                   transactionType: transactionType.value,
                   amount: doc.totalTime * activityType.rate,
                   paymentType: paymentType,
               }});
              } else {
                Transactions.insert({
                       personId: doc.personId,
                       timelogId: doc._id,
                       date: doc.endTime,
                       transactionType: transactionType.value,
                       amount: doc.totalTime * activityType.rate,
                      paymentType: paymentType || "pending",
                   }, function(err, transactionId){
                    if (err){
                        console.error(err);
                    } else {
                        Timelogs.direct.update({_id:doc._id}, {$set:{transactionId: transactionId}});
                    }
                   });
                 }
              }
             //Update the person's latest sign-out
             var person = People.findOne(doc.personId);
             if (doc.endTime > (person.lastSignout || 0)){
                 People.direct.update(doc.personId, {$set:{lastSignout: doc.endTime}}, function(err, i){console.log(err, i)});
             }
          }


});


Timelogs.after.remove(function (userId, doc) {
    //remove the associated transaction, if there is one
    if(Meteor.isServer) {
        if (doc.transactionId) {
            Transactions.direct.remove({_id: doc.transactionId});
        }
    }
});



Timelogs.attachSchema(Schemas.Timelogs);

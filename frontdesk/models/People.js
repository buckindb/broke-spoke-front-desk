People = new Meteor.Collection("people");


Schemas = (typeof Schemas === 'undefined') ? {} : Schemas;

//Addresses, used for person, emergency contact, parent
Schemas.Address = new SimpleSchema({
  street: {
    type: String,
    max: 100,
    optional:true
  },
  city: {
    type: String,
    max: 50,
    defaultValue:"Lexington",
    optional:true
  },
  state: {
    type: String,
    defaultValue: "KY",
    optional:true
  },
  zip: {
    type: String,
    regEx: /^[0-9]{5}$/,
    optional:true
  }
});


Schemas.People = new SimpleSchema({
    firstName: {
        type: String,
        label: "First name",
        autoValue: function(){
            if (this.value){
                return this.value[0].toUpperCase() + this.value.slice(1);
            }
        }
    } ,
    lastName: {
        type:String,
        label: "Last name",
        autoValue: function(){
            if (this.value) {
                return this.value[0].toUpperCase() + this.value.slice(1);
            }
        }
    },

    middleInitial: {
        type:String,
        label: "Middle name or initial",
        autoValue: function(){
            if (this.value) {
                return this.value[0].toUpperCase() + this.value.slice(1);
            }
        },
        optional:true,
        //custom: function(){
        //    if (this.isInsert && !this.isSet){
        //        return 'middleInitial';
        //    }
        //}
    },
   
    email: {
        type: String, 
        label: "E-mail",
        regEx: SimpleSchema.RegEx.Email,
        optional: true
    },
   
    sweatEquity: {
      type:Number,
      optional: true,
      autoValue: function(){
          if (this.value > 250){
              return 250;
          } else if (this.value == null){
              //this conditional is probably causing SE to be reset to zero on basicInfo update
              //Currently handled by including SE as a hidden field on the form.
              return 0;
          }
      }
    },
    
    waiverAcceptDate: {
        type:Date,
        label:"Waiver accepted on",
        optional:true,
    },

    membershipExpiration: {
        type:Date,
        label:"Membership Expiration",
        optional:true,
    },
    
    //CONTACT INFO
    address: {
      type:Schemas.Address,
      optional:true
    },

    phone: {
        type:String,
        label:"Phone",
        optional:true
    },
    
    // Emergency Contact (will be parent in case of minor)
    emergencyContactName: {
        type:"String",
        label:"Emergency Contact Name",
        optional:true
    },
    
    emergencyContactPhone: {
        type:"String",
        label:"Emergency Contact Phone",
        optional:true
    },
    
    
    emergencyContactAddress: {
        type:Schemas.Address,
        optional:true
    },
    
    emergencyContactRelation: {
        type: "String",
        label:"Relation",
        optional:true
    },
    
    //Fields for minors
    birthdate: {
        type: Date,
        label: "Birthdate",
        optional:true
    },

    imported: {
        type: Boolean,
        optional:true
    },
    
    //Training Data
    //TODO: add volunteer assignment hourly totals
    
    //Metadata
    createdOn: {
        type:Date,
        label: "Date created",
        autoValue: function() {
        if (this.isInsert) {
            if (this.value){
                return this.value;
            }
          return new Date;
        } else if (this.isUpsert) {
          return {$setOnInsert: new Date};
        } else {
          this.unset();
        }
      }
    },
    lastSignout: {
        type:Date,
        label: "Last seen",
        optional:true
    },

    contacts: {
        type: Array,
        optional: true
    },
    'contacts.$': {
        type: Object
    },
    'contacts.$.name': {
        type: String
    },
    'contacts.$.phone': {
        type: String
    }
   
});

Schemas.People.messages({
    'middleInitial': "Please provide a middle initial."
})


//permissions
People.allow({
    insert: function(userId, person){
        return userId;
    },
    update: function(userId, person){
        return userId;
    },
    remove: function(userId, person){
        return userId;
    }
});

People.before.update(function (userId, doc, fieldNames, modifier, options) {
    console.log('beforerUpdate', doc.sweatEquity);
    //modifier.$set = modifier.$set || {};
    //modifier.$set.sweatEquity = doc.sweatEquity;
});


//Document syncing
People.after.update(function (userId, personDoc, fieldNames, modifier, options) {
    console.log('afterUpdate', personDoc.sweatEquity);
   // this is a loop, it calls People.update
   Meteor.call('peopleUpdateTimelogs', personDoc);
   Meteor.call('peopleUpdateTransactions', personDoc);
});

People.after.remove(function (userId, personDoc) {
   Meteor.call('peopleRemoveTimelogs', personDoc);
   Meteor.call('peopleRemoveTransactions', personDoc);
});


People.attachSchema(Schemas.People);

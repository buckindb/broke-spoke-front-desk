
Router.map(function(){
  var that = this;
  this.route('login',{
    // path:'/login',
    template: 'login',
    layoutTemplate:'fluidRowLayout'
  });

    this.route('unauthorized',{
     //path:'/unauthorized',
    template: 'unauthorized',
    layoutTemplate:'fluidRowLayout'
  })

    this.route('homepage', {
        path: '/',
        fastRender:true,
        layoutTemplate:'fluidRowLayout'
    });

    this.route('adminHome', {
        path: '/admin',
        controller: 'AdminListController',
        fastRender:true
    });


  this.route('admin', {
    path: '/admin/dashboard',
    controller: 'AdminListController',
    template: 'adminDashboard',
    fastRender:true
  });

    this.route('users', {
    path: '/admin/users',
    template: 'usersList',
    controller: 'AdminListController',
    layoutTemplate: 'adminLayout',
  });

  //People
  this.route('peopleAdminList', {
    path: '/admin/people/',
    controller: 'AdminListController',
    layoutTemplate: 'adminLayout',
  })

  this.route('peopleAdminNew', {
    path: '/admin/people/new',
    controller: 'AdminController',
    layoutTemplate: 'adminLayout',
  })

  this.route('peopleAdminEdit', {
    path: '/admin/people/:personId',
    controller: 'AdminController',
    layoutTemplate: 'adminLayout',

    data: function(){
      var data = this.adminData();
      data.person = People.findOne({_id: this.params.personId});
      return data;
    },

    waitOn: function() {
    // return one handle, a function, or an array
    return Meteor.subscribe('peopleRecord', {_id:this.params.personId});
    },

    action: function () {
      // this.ready() is true if all items returned from waitOn are ready
      if (this.ready())
        Session.set('timelogsFilter', {personId: this.params.personId});
        this.render();
      }
  })

  this.route('peopleAdminEditTimelog', {
        path: '/admin/people/:personId/timelog/:timelogId',
        controller: 'AdminController',
        layoutTemplate: 'adminLayout',
        template: 'timelogsAdminEdit',
        data: function(){
            var data = this.adminData();
            data.timelog = Timelogs.findOne({_id: this.params.timelogId});

            data.person = People.findOne(this.params.personId)
            return data;
        },
        waitOn: function () {
            // return one handle, a function, or an array
            return Meteor.subscribe('timelogRecord', {_id:this.params.timelogId});
        },

        action: function () {
            if (this.ready())
                this.render();
        }
    });

    this.route('peopleAdminNewTimelog', {
        path: '/admin/people/:personId/new-timelog',
        controller: 'AdminController',
        layoutTemplate: 'adminLayout',
        template:'timelogsAdminNew',
        data: function(){
            var data = this.adminData();
            data.person = People.findOne(this.params.personId)
            return data;
        },
        waitOn: function () {
            // return one handle, a function, or an array
            return Meteor.subscribe('peopleRecord', {_id:this.params.personId});
        },


        action: function () {
            if (this.ready())
                this.render();
        }
    });


    this.route('peopleAdminEditTransaction', {

        path: '/admin/people/:personId/transactions/:transactionId',
            controller: 'AdminController',
            layoutTemplate: 'adminLayout',
            template:'transactionsAdminEdit',
            data: function(){
            var data = this.adminData();
                data.person = People.findOne(this.params.personId);

            //if the transaction is a debit, make the balance positive while editing it in the client
            data.transaction = Transactions.findOne({_id: this.params.transactionId});
            if (data.transaction &&  data.transaction.amount){
                var transactionType = appSettings.transactionTypes.get(data.transaction.transactionType);
                //if (transactionType.direction ==="debit"){
                //    data.transaction.amount = Math.abs(data.transaction.amount);
                //}
            }

            return data;
        },
        waitOn: function () {
            // return one handle, a function, or an array
            return [Meteor.subscribe('transactionRecord', {_id:this.params.transactionId}), Meteor.subscribe('peopleRecord', {_id:this.params.personId})];
        },

        action: function () {
            // this.ready() is true if all items returned from waitOn are ready
            if (this.ready())
                this.render();
        }

    });

    this.route('peopleAdminNewTransaction', {

        path: '/admin/people/:personId/new-transaction',
        controller: 'AdminController',
        layoutTemplate: 'adminLayout',
        template:'transactionsAdminNew',
        data: function(){
            var data = this.adminData();
            data.person = People.findOne(this.params.personId)
            return data;
        },
        waitOn: function () {
            // return one handle, a function, or an array
            return Meteor.subscribe('peopleRecord', {_id:this.params.personId});
        },

        action: function () {
            // this.ready() is true if all items returned from waitOn are ready
            if (this.ready())
                this.render();
        }

    });

  //Transactions
  this.route('transactionsAdminList', {
    path: '/admin/transactions/',
    controller: 'AdminListController',
    layoutTemplate: 'adminLayout',
  })

  this.route('transactionsAdminNewTransaction', {
    path: '/admin/transactions/new',
    controller: 'AdminController',
    layoutTemplate: 'adminLayout',
      template:'transactionsAdminNew'
  })

  this.route('transactionsAdminEditTransaction', {
    path: '/admin/transactions/edit/:transactionId',
    controller: 'AdminController',
    layoutTemplate: 'adminLayout',
      template:'transactionsAdminEdit',
    data: function(){
      var data = this.adminData();
      data.transaction = Transactions.findOne({_id: this.params.transactionId});

        //if the transaction is a debit, make the balance positive while editing it in the client
        data.transaction = Transactions.findOne({_id: this.params.transactionId});
        if (data.transaction &&  data.transaction.amount){
            var transactionType = appSettings.transactionTypes.get(data.transaction.transactionType);
            //if (transactionType.direction ==="debit"){
            //    data.transaction.amount = Math.abs(data.transaction.amount);
            //}
        }

      return data;
    },
    waitOn: function () {
    // return one handle, a function, or an array
    return Meteor.subscribe('transactionRecord', {_id:this.params.transactionId});
    },

    action: function () {
      // this.ready() is true if all items returned from waitOn are ready
      if (this.ready())
        this.render();
      }
  })

   //Timelogs
  this.route('timelogsAdminList', {
    path: '/admin/timelogs/',
    controller: 'AdminListController',
    layoutTemplate: 'adminLayout',
  })

  this.route('timelogsAdminNewTimelog', {
    path: '/admin/timelogs/new',
    controller: 'AdminController',
    layoutTemplate: 'adminLayout',
      template:'timelogsAdminNew'
  })

  this.route('timelogsAdminEditTimelog', {
    path: '/admin/timelogs/edit/:timelogId',
    controller: 'AdminController',
    layoutTemplate: 'adminLayout',
      template: 'timelogsAdminEdit',
    data: function(){
      var data = this.adminData();
      data.timelog = Timelogs.findOne({_id: this.params.timelogId});
      return data;
    },
        waitOn: function () {
     // return one handle, a function, or an array
         return Meteor.subscribe('timelogRecord', {_id:this.params.timelogId});
    },

    action: function () {
      // this.ready() is true if all items returned from waitOn are ready
      if (this.ready())
        this.render();
      }
  })

    //Reports
    this.route('reportsAdminList', {
        path: '/admin/reports/',
        controller: 'AdminController',
        layoutTemplate: 'adminLayout',
    })



    //Bikes
    this.route('bikesAdminList', {
        path: '/admin/bikes/',
        controller: 'AdminListController',
        layoutTemplate: 'adminLayout',
    })

    this.route('bikesAdminNew', {
        path: '/admin/bikes/new',
        controller: 'AdminController',
        layoutTemplate: 'adminLayout',
        template:'bikesAdminNew'
    })

    //signin
    this.route('signInHome', {
        path: '/signin',
        layoutTemplate: 'SignInLayout',
        data: function(){

        },
        waitOn: function () {

        },

        action: function () {
            if (this.ready())
                this.render();
        }
    })

 });

//Router Permissions
// Would be nice to add these as attributes on route,
//but I can't find how to get into the router object
var routePermissions = {
    'app-admin':[
      'admin',
        'adminHome',
      'users',
      'signInHome',
      'peopleAdminList',
      'peopleAdminNew',
      'peopleAdminEdit',
      'peopleAdminEditTimelog',
      'peopleAdminNewTimelog',
      'timelogsAdminList',
      'timelogsAdminNew',
      'timelogsAdminEdit',
      'transactionsAdminList',
      'transactionsAdminNew',
      'transactionsAdminEdit',
      'timelogsAdminList',
      ],

    'shop-admin':[
        'admin',
        'adminHome',
        'users',
        'signInHome',
        'peopleAdminList',
        'peopleAdminNew',
        'peopleAdminEdit',
        'peopleAdminEditTimelog',
        'peopleAdminNewTimelog',
        'timelogsAdminList',
        'timelogsAdminNew',
        'timelogsAdminEdit',
        'transactionsAdminList',
        'transactionsAdminNew',
        'transactionsAdminEdit',
        'timelogsAdminList',
      ],

    'signin-kiosk':[
      'signInHome'
    ]
}

var permissionsByRoute = function(){
  var output = {};
  _.each(routePermissions, function(routes, role){
    _.each(routes, function(route, i, list){
      output[route] = output[route] || [];
      output[route].push(role);
    })
  })
  return output;
}();

//Router Hooks from
//http://www.manuel-schoebel.com/blog/meteorjs-iron-router-filters-before-and-after-hooks

var IR_BeforeHooks = {

    authorize: function(pause){
        console.log('IR before hook');
      var that = this;
      Session.set('adminSection', that.route._path.split('/')[2]);
        //check if the route is protected;
        var protectedRoutes = _.flatten(_.values(routePermissions));
        var routeName = this.route.getName();

        if (_.indexOf(protectedRoutes, routeName) > -1){
          if (!(Meteor.loggingIn() || Meteor.user())) {
             //authenticate user
             this.layout('fluidRowLayout');
             this.render('login');
              return pause();
          }
          else {
              //authorize user
              var permittedRoles = permissionsByRoute[routeName];

              if (Roles.userIsInRole(Meteor.userId(), permittedRoles)){
                this.next();
              } else {
                this.render('unauthorized');
              }
          }

        } else { //not protected
            this.next();
        }
    },

    section:function(){
      this.next();
    }
}



Router.onBeforeAction(IR_BeforeHooks.authorize);

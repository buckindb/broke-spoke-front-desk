appSettings = {
    activityTypes: {
        items: [
               { label:"Volunteering", value:"volunteering", rate:8, transactionType:'volunteer-credit'},
               { label:"Member Stand Time", value:"member-stand-time", rate:0},
               { label:"Stand Time", value:"stand-time", rate:-4, transactionType:'stand-time-purchase'},
               { label:"Shopping for Parts/Bikes", value:"shopping", rate:0},
               { label:"Other", value:"other", rate:0},
               { label:"Imported login", value:"imported", rate:0},
            ],
        get: function(handle){
            return _.findWhere(this.items, {'value': handle});
        }
        
    },
    transactionTypes: {
        items: [
                { label:"Volunteer Credit", value:"volunteer-credit", direction:"credit", paymentType:"equity"},
                { label:"Stand Time Purchase", value:"stand-time-purchase",  direction:"debit", paymentType:"both"},
                { label:"Parts Purchase", value:"parts-purchase",  direction:"debit", paymentType:"both"},
                { label:"Bike Purchase", value:"bike-purchase",  direction:"debit",  paymentType:"both"},
                { label:"Imported balance", value:"imported",  direction:"credit",  paymentType:"equity"},
                
            ],
         get: function(handle){
            return _.findWhere(this.items, {'value': handle});
        }
    },
    bikeStatusTypes: {
        items: [
            {label:"Needs assessment", value:'needs-assessment'},
            {label:"Needs price", value:"needs-price"},
            {label:"For sale", value:"for-sale"},
            {label:"On hold", value:"on-hold"},
            {label:"Sold", value:"sold"},
            {label:"Customer Storage", value:"customer-storage"}
        ],
        get: function(handle){
            return _.findWhere(this.items, {'value': handle});
        }
    },
    //done because Meteor has no method for restoring session var defaults
    //Would be better to get rid of the state with url params
    defaultVars: {
        items: {
            peopleCursor: 0,
            peopleFilter: {},
            peopleSort: {lastName:1},
            peoplePageLength: 15,
            //timelogs
            timelogsCursor: 0,
            timelogsFilter: {},
            timelogsSort: {startTime:-1},
            timelogsPageLength: 15,
            //transactions
            transactionsCursor: 0,
            transactionsFilter: {paymentStatus:{$ne:'pending'}},
            transactionsSort: {date:-1},
            transactionsPageLength: 15,
            //bikes
            bikesCursor: 0,
            bikesFilter:{},
            bikesSort: {dateAdded:-1},
            bikesPageLength: 15
            
        },
        setDefaults: function(vars){
            var that = this;
            _.each(vars, function(varName, i, list){
                Session.set(varName, that.items[varName]);
            })
        }
    }
};

